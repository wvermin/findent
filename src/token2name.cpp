#include <iostream>
#include <string>
#include "parser.hpp"
#include "token2name.h"
#include "functions.h"
std::string t2n(int token)
{
   std::string num = number2string(token);
   if (firsttoken == 0 || lasttoken == 0 || token < firsttoken || token > lasttoken)
      return "*" + num + "*";
#ifdef HAVE_TOKEN_LIST
   return "*"+tokenlist[token-firsttoken]+"*"+"("+num+")";
#else
      return "*" + num + "*";
#endif
}
