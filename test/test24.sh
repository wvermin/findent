#!/bin/sh
# -copyright-
#-# Copyright: 2015-2025 Willem Vermin wvermin@gmail.com
#-# 
#-# License: BSD-3-Clause
#-#  Redistribution and use in source and binary forms, with or without
#-#  modification, are permitted provided that the following conditions
#-#  are met:
#-#  1. Redistributions of source code must retain the above copyright
#-#     notice, this list of conditions and the following disclaimer.
#-#  2. Redistributions in binary form must reproduce the above copyright
#-#     notice, this list of conditions and the following disclaimer in the
#-#     documentation and/or other materials provided with the distribution.
#-#  3. Neither the name of the copyright holder nor the names of its
#-#     contributors may be used to endorse or promote products derived
#-#     from this software without specific prior written permission.
#-#   
#-#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#-#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#-#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#-#  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
#-#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#-#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#-#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#-#  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
#-#  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#-#  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#-#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# testing error conditions of relabeling

if test -e prelude ; then
   . ./prelude
else
   . ./debian/tests/prelude
fi
rc=0
cat << eof > prog
subroutine s
goto 10
10 continue
end
   program p
     goto 123 
  end
eof
cat << eof > expect
subroutine s
   goto 1000
1000 continue
end
program p
   goto 123
end
eof
../doit "--relabel" "-ifree" " Test001: undefined label format:free"
rc=`expr $rc + $?`

cat << eof > prog
         subroutine s
         goto 10
         10 continue
         end
      program p
       goto 123 
       end
eof
cat << eof > expect
      subroutine s
         goto 1000
 1000    continue
      end
      program p
         goto 123
      end
eof
../doit "--relabel" "-ifixed" " Test002: undefined label format: fixed"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s
goto 10
10 continue
end
   program p
     goto 123 
  end
eof
cat << eof > expect
!Relabeling error: undefined label: 123 (6);
eof
../doit "--query-relabel=1" "-ifree" " Test003: undefined label format:free"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s;continue
goto 10
10 continue
end
   program p
     goto 123 
123 continue
  end
eof
cat << eof > expect
subroutine s;continue
   goto 10
10 continue
end
program p
   goto 123
123 continue
end
eof
../doit "--relabel" "-ifree" " Test004: subroutine in multistatemen line: free"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s;continue
      goto 10
10     continue
      end
         program p
           goto 123 
123      continue
        end
eof
cat << eof > expect
      subroutine s;continue
         goto 10
   10    continue
      end
      program p
         goto 123
  123    continue
      end
eof
../doit "--relabel" "-ifixed" " Test005: subroutine in multistatemen line: fixed"

cat << eof > prog
      subroutine s;continue
      goto 10
10     continue
      end
         program p
           goto 123 
123      continue
        end
eof
cat << eof > expect
!Relabeling error: subroutine statement part of multi statement line (1);
eof
../doit "--query_relabel=1" "-ifixed" " Test006: subroutine in multistatemen line: fixed"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s
function f(x)
goto 10
10 continue
end
   program p
     goto 123 
123 continue
  end
eof
cat << eof > expect
subroutine s
   function f(x)
      goto 10
10    continue
   end
   program p
      goto 123
123   continue
   end
eof
../doit "--relabel" "-ifree" " Test007: misplaced function definition: free"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s
            goto 10
       continue
         end
         program p
         subroutine f(x)
            goto 123
      continue
         end
eof
cat << eof > expect
      subroutine s
         goto 10
         continue
      end
      program p
         subroutine f(x)
            goto 123
            continue
         end
eof
../doit "--relabel" "-ifixed" " Test008: misplaced subroutine definition: fixed"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s
goto 10
10 continue
end
   program p
   function f(x)
     goto 123 
123 continue
  end
eof
cat << eof > expect
!subroutine s:
!original(def'd )     new (used)
!      10(     3) ->  1000(2)
!Relabeling error: misplaced function statement (6);
eof
../doit "--query-relabel=2" "-ifree" " Test009: misplaced function definition: free"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s
goto (10,20,x),i
10 continue
20 continue
end
   program p
     goto 123 
123 continue
  end
eof
cat << eof > expect
subroutine s
   goto (10,20,x),i
10 continue
20 continue
end
program p
   goto 123
123 continue
end
eof
../doit "--relabel" "-ifree" " Test010: computed goto: free"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s
      goto (10,20,x),i
10       continue
 20      continue
      end
         program p
           goto 123 
123       continue
        end
eof
cat << eof > expect
!Relabeling error: error in computed GOTO list (2);
eof
../doit "--query-relabel=1" "-ifixed" " Test011: computed goto: fixed"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s
      assign 20 to i
      goto i,(10,20,)
10       continue
 20      continue
      end
         program p
           goto 123 
123       continue
        end
eof
cat << eof > expect
      subroutine s
         assign 20 to i
         goto i,(10,20,)
   10    continue
   20    continue
      end
      program p
         goto 123
  123    continue
      end
eof
../doit "--relabel" "-ifixed" " Test012: assigned goto: fixed"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s
      assign 20 to i
      goto i,(10,20,)
10       continue
 20      continue
      end
         program p
           goto 123 
123       continue
        end
eof
cat << eof > expect
!Relabeling error: error in assigned GOTO list (3);
eof
../doit "--query_relabel=1" "-ifixed" " Test013: assigned goto: fixed"
rc=`expr $rc + $?`

cat << eof > prog
subroutine s
goto (10,20),i
10 continue
20 continue
end
   program p
     goto 12&
     &3 
123 continue
  end
eof
cat << eof > expect
subroutine s
   goto (1000,1010),i
1000 continue
1010 continue
end
program p
   goto 12&
   &3
123 continue
end
eof
../doit "--relabel" "-ifree" " Test014: continuation in label: free"
rc=`expr $rc + $?`

cat << eof > prog
         subroutine s
         goto (10,20),i
10          continue
20          continue
         end
            program p
              goto 12
     &3 
123          continue
           end
eof
cat << eof > expect
!subroutine s:
!original(def'd )     new (used)
!      10(     3) ->  1000(2)
!      20(     4) ->  1010(2)
!Relabeling error: label not on one line: 123(8)
eof
../doit "--query_relabel --query-relabel=2" "-ifixed" " Test015: continuation in label: fixed"
rc=`expr $rc + $?`


cat << eof > prog
subroutine s
goto (10,20,30),i
10 continue
20 continue
30 continue
end
   program p
     goto (123,&
#ifdef X
     1)
#else
     2)
#endif
123 continue
1 continue
2 continue
  end
eof
cat << eof > expect
subroutine s
   goto (1000,1010,1020),i
1000 continue
1010 continue
1020 continue
end
program p
   goto (123,&
#ifdef X
      1)
#else
   2)
#endif
123 continue
1  continue
2  continue
end
eof
../doit "--relabel" "-ifree" " Test016: intervening preprocessor statement: free"
rc=`expr $rc + $?`

cat << eof > prog
      subroutine s
      goto (10,20,30),i
10         continue
20         continue
30         continue
      end
       program p
       goto (123,
#ifdef X
     &1)
#else
     &2)
#endif
123 continue
1 continue
2 continue
  end
eof
cat << eof > expect
!Relabeling error: intervening preprocessor statement (12);
eof
../doit "--query-relabel=1" "-ifixed" " Test017: intervening preprocessor statement: fixed"
rc=`expr $rc + $?`

cat << eof > prog
         subroutine s
         goto (10,20),i
10          continue
20          continue
         end
            program p
              goto 123
123          continue
1          continue
4          continue
5          continue
9          continue
10       continue
           end
eof
cat << eof > expect
      subroutine s
         goto (99990,99992),i
99990    continue
99992    continue
      end
      program p
         goto 123
  123    continue
    1    continue
    4    continue
    5    continue
    9    continue
   10    continue
      end
eof
../doit "--relabel=99990,2" "-ifixed" " Test018: label too large: fixed"
rc=`expr $rc + $?`


cat << eof > prog
 subroutine s
         goto (10,20),i
10          continue
20          continue
         end
            program p
              goto 123
123          continue
1          continue
4          continue
5          continue
9          continue
10         continue
           end
eof
cat << eof > expect
!Relabeling error: label would go out of range: 5 -> 100000 (11);
eof
../doit "--query-relabel=1" "-ifree --relabel=99990,2" " Test019: label too large: free"
rc=`expr $rc + $?`

cat << eof > prog
         subroutine s
         goto (10,20),i
10          continue
20          continue
         end; program p
              goto 123
123          continue
           end
eof
cat << eof > expect
      subroutine s
         goto (10,20),i
   10    continue
   20    continue
      end; program p
         goto 123
  123    continue
      end
eof
../doit "--relabel" "-ifixed" " Test020: end statement in multi statement line: fixed"
rc=`expr $rc + $?`

cat << eof > prog
  subroutine s
         goto (10,20),i
10          continue
20          continue
  end; program p
              goto 123
123          continue
           end
eof
cat << eof > expect
!Relabeling error: END in multi statement line (5);
eof
../doit "--relabel" "-ifree --query-relabel=1" " Test021: end statement in multi statement line: free"
rc=`expr $rc + $?`

cp ../progrelabelfree.f prog
cat << eof > expect
!function point_dist:
!original(def'd )     new (used)
!procedure fun:
!original(def'd )     new (used)
!function fun:
!original(def'd )     new (used)
!subroutine x:
!original(def'd )     new (used)
!       1(    25) ->  1000(24)
!procedure point_dist:
!original(def'd )     new (used)
!      10(    31) ->  1010(30)
!subroutine mysub:
!original(def'd )     new (used)
!subroutine mysubomp:
!original(def'd )     new (used)
!      10(    45) ->  1020(42,43)
!subroutine mysub1:
!original(def'd )     new (used)
!       1(    49) ->  1030()
!subroutine mysub2:
!original(def'd )     new (used)
!function myfun:
!original(def'd )     new (used)
!      10(    59) ->  1040(57)
!      20(    61) ->  1050(57)
!      30(    62) ->  1060(57)
!      50(    67) ->  1070(59,61,62,64)
!program p:
!original(def'd )     new (used)
!       1(    80) ->  1160(77)
!    1000(    98) ->  1220(97)
!    1001(    98) ->  1230(96)
!      11(    76) ->  1110()
!    1101(   132) ->  1340(122,125,126,127)
!    1102(   135) ->  1350(125,130)
!     111(    75) ->  1080()
!     112(    75) ->  1090()
!     115(    75) ->  1100()
!     120(   101) ->  1240(100)
!    1200(   131) ->  1330(132,133,134,135,136,138,140)
!   12345(    89) ->  1190(87)
!     130(   102) ->  1250(100,125)
!       2(    80) ->  1150(77)
!    2010(   185) ->  1360(130,138,140,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,167,167,167,168,168,168,169,170,174,174,174,176,176,181,181)
!    2020(   186) ->  1370(145,146,164,167,171,174,177,178,179,182,183,184)
!       3(    79) ->  1140(77)
!    3010(   203) ->  1380(187,189,193,194,195,199,200,201)
!    3020(   204) ->  1390(187,189,193,194,195,199,200,201)
!    3030(   205) ->  1400(187,189,193,194,195,199,200,201)
!      31(    76) ->  1120()
!       5(    86) ->  1180(86)
!      51(    76) ->  1130()
!     511(    84) ->  1170(81,82,83)
!       8(    92) ->  1200(90)
!       9(    95) ->  1210(93)
!      91(   116) ->  1300(104,104,108,108,111,122,125,127,130)
!      92(   117) ->  1310(104,104,108,108,111,122,125,127,130)
!      93(   118) ->  1320(104,104,108,108,112,122,125,127,130)
!      95(   104) ->  1260()
!     951(   108) ->  1280()
!      96(   104) ->  1270()
!     961(   108) ->  1290()
!function f:
!original(def'd )     new (used)
!       1(   208) ->  1410()
!subroutine subf:
!original(def'd )     new (used)
!       1(   212) ->  1420()
!subroutine subx:
!original(def'd )     new (used)
!     100(   264) ->  1490(232,233,234,235,236,237,238,240,241,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263)
!     101(   223) ->  1440(222)
!     102(   225) ->  1450(224)
!     103(   227) ->  1460(226)
!     104(   229) ->  1470(228)
!     105(   231) ->  1480(230)
!     110(   264) ->  1500(220,221,236,238,239,240,242,245,246,247,253,256,259,261,263)
!     120(   264) ->  1510(220,221,236,245,246,247,253,256,259,261,263)
!     130(   264) ->  1520(220,253,256)
!      99(   219) ->  1430()
!subroutine subx1:
!original(def'd )     new (used)
!     100(   302) ->  1530(269,270,271,272,273,274,275,277,278,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300)
!     110(   302) ->  1540(273,275,276,277,279,282,283,284,290,293,296,298,300)
!     120(   302) ->  1550(273,282,283,284,290,293,296,298,300)
!     130(   302) ->  1560(290,293)
!subroutine suby:
!original(def'd )     new (used)
!     100(   351) ->  1630(320,321,322,323,324,325,327,328,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350)
!     101(   311) ->  1580(310)
!     102(   313) ->  1590(312)
!     103(   315) ->  1600(314)
!     104(   317) ->  1610(316)
!     105(   319) ->  1620(318)
!     110(   351) ->  1640(308,309,323,325,326,327,329,332,333,334,340,343,346,348,350)
!     120(   351) ->  1650(308,309,323,332,333,334,340,343,346,348,350)
!     130(   351) ->  1660(308,340,343)
!      99(   307) ->  1570()
!subroutine suby1:
!original(def'd )     new (used)
!     100(   388) ->  1670(356,357,358,359,360,361,363,364,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386)
!     110(   388) ->  1680(359,361,362,363,365,368,369,370,376,379,382,384,386)
!     120(   388) ->  1690(359,368,369,370,376,379,382,384,386)
!     130(   388) ->  1700(376,379)
!subroutine old_dec:
!original(def'd )     new (used)
!      10(   414) ->  1710(397,402,407)
!      20(   415) ->  1720(409,410)
!      30(   416) ->  1730(412)
!      40(   417) ->  1740(413)
!      50(   418) ->  1750(398,399,400,401,402,403,404,405,405,412)
!      60(   419) ->  1760(405,413)
!Relabeling error: intervening preprocessor statement (427);
eof
../doit "--query_relabel --query-relabel=2" "" " Test022: query-relabel of larger program: free"
rc=`expr $rc + $?`

cp ../progfixedrelabelfixed.f prog
cat << eof > expect
!function point_dist:
!original(def'd )     new (used)
!procedure fun:
!original(def'd )     new (used)
!function fun:
!original(def'd )     new (used)
!subroutine x:
!original(def'd )     new (used)
!       1(    25) ->  1000(24)
!procedure point_dist:
!original(def'd )     new (used)
!      10(    31) ->  1010(30)
!subroutine mysub:
!original(def'd )     new (used)
!subroutine mysubomp:
!original(def'd )     new (used)
!      10(    44) ->  1020(41,42)
!subroutine mysub1:
!original(def'd )     new (used)
!       1(    48) ->  1030()
!subroutine mysub2:
!original(def'd )     new (used)
!function myfun:
!original(def'd )     new (used)
!      10(    58) ->  1040(56)
!      20(    60) ->  1050(56)
!      30(    61) ->  1060(56)
!      50(    65) ->  1070(58,60,61,63)
!program p:
!original(def'd )     new (used)
!       1(    79) ->  1120(75)
!    1000(    98) ->  1180(97)
!    1001(    99) ->  1190(96)
!      11(    74) ->  1090()
!    1101(   138) ->  1300(128,131,132,133)
!    1102(   142) ->  1310(131,136)
!     111(    73) ->  1080()
!     120(   102) ->  1200(101)
!    1200(   137) ->  1290(138,139,140,141,142,143,145,147)
!   12345(    89) ->  1150(87)
!     130(   103) ->  1210(101,131)
!       2(    78) ->  1110(75)
!    2010(   193) ->  1320(136,145,147,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,174,174,174,175,175,176,177,178,182,182,182,184,184,189,189)
!    2020(   194) ->  1330(152,153,171,174,179,182,185,186,187,190,191,192)
!       3(    77) ->  1100(75)
!    3010(   211) ->  1340(195,197,201,202,203,207,208,209)
!    3020(   212) ->  1350(195,197,201,202,203,207,208,209)
!    3030(   213) ->  1360(195,197,201,202,203,207,208,209)
!       5(    86) ->  1140(85)
!     511(    83) ->  1130(80,81,82)
!       8(    92) ->  1160(90)
!       9(    95) ->  1170(93)
!      91(   122) ->  1260(105,106,111,112,117,128,131,133,136)
!      92(   123) ->  1270(105,106,111,112,117,128,131,133,136)
!      93(   124) ->  1280(105,107,111,113,118,128,131,133,136)
!      95(   106) ->  1220()
!     951(   112) ->  1240()
!      96(   107) ->  1230()
!     961(   113) ->  1250()
!function f:
!original(def'd )     new (used)
!      28(   217) ->  1370(216)
!subroutine subx:
!original(def'd )     new (used)
!      29(   222) ->  1380(221)
!subroutine subx1:
!original(def'd )     new (used)
!     100(   275) ->  1450(242,243,244,245,246,247,248,250,251,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273)
!     101(   233) ->  1400(232)
!     102(   235) ->  1410(234)
!     103(   237) ->  1420(236)
!     104(   239) ->  1430(238)
!     105(   241) ->  1440(240)
!     110(   276) ->  1460(230,231,246,248,249,250,252,255,256,257,263,266,269,271,273)
!     120(   277) ->  1470(230,231,246,255,256,257,263,266,269,271,273)
!     130(   278) ->  1480(230,263,266)
!      99(   229) ->  1390()
!subroutine subx2:
!original(def'd )     new (used)
!     100(   317) ->  1490(284,285,286,287,288,289,290,292,293,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315)
!     110(   318) ->  1500(288,290,291,292,294,297,298,299,305,308,311,313,315)
!     120(   319) ->  1510(288,297,298,299,305,308,311,313,315)
!     130(   320) ->  1520(305,308)
!subroutine suby1:
!original(def'd )     new (used)
!     100(   371) ->  1590(339,340,341,342,343,344,346,347,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369)
!     101(   330) ->  1540(329)
!     102(   332) ->  1550(331)
!     103(   334) ->  1560(333)
!     104(   336) ->  1570(335)
!     105(   338) ->  1580(337)
!     110(   372) ->  1600(327,328,342,344,345,346,348,351,352,353,359,362,365,367,369)
!     120(   373) ->  1610(327,328,342,351,352,353,359,362,365,367,369)
!     130(   374) ->  1620(327,359,362)
!      99(   326) ->  1530()
!subroutine suby2:
!original(def'd )     new (used)
!     100(   412) ->  1630(379,380,381,382,383,384,386,387,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409)
!     110(   413) ->  1640(382,384,385,386,388,391,392,393,399,402,405,407,409)
!     120(   414) ->  1650(382,391,392,393,399,402,405,407,409)
!     130(   415) ->  1660(399,402)
!subroutine old_dec:
!original(def'd )     new (used)
!      10(   440) ->  1670(425,431,433)
!      20(   441) ->  1680(429,430,435,436)
!      30(   442) ->  1690(438)
!      40(   443) ->  1700(439)
!      50(   444) ->  1710(421,422,423,424,425,426,427,428,428,438)
!      60(   445) ->  1720(428,439)
!Relabeling error: intervening preprocessor statement (455);
eof
../doit "--query_relabel --query-relabel=2" "" " Test023: query-relabel of larger program: fixed"
rc=`expr $rc + $?`

cat << eof > prog
  subroutine s
         goto (10,20),i
10          continue
20          continue
  end
  program p
              goto 123
123          continue
continue;contains
real function f(x)
20 continue
end function f
           end program p
eof
cat << eof > expect
!Relabeling error: CONTAINS in multi statement line (9);
eof
../doit "--relabel" "-ifree --query-relabel=1" " Test024: contains statement in multi statement line: free"
rc=`expr $rc + $?`

cat << eof > prog
         subroutine s
         goto (10,20),i
10          continue
20          continue
               end
            program p
              goto 123
123          continue
         contains; real function f(x)
20         continue
          f=10*x
          end function f
           end program p
eof
cat << eof > expect
!subroutine s:
!original(def'd )     new (used)
!      10(     3) ->  1000(2)
!      20(     4) ->  1010(2)
!Relabeling error: CONTAINS in multi statement line (9);
eof
../doit "--relabel" "-ifixed --query-relabel=2" " Test025: contains statement in multi statement line: fixed"
rc=`expr $rc + $?`

cat << eof > prog
program paren
   x = y + &
      fun(a, &
      abcd ,&
      z)
   x = y +  &
              z + &
      a; call sub (a, &
!comment
      b, &
      c)
   x = y +  a; call sub (a, &
      b, &
      c)

   print *,'abc'; call sub(a,&
      b,&
      c)
#if 0
   write(10,'20) x; call sub(a,&
      b,&
      c)
   '
   write(10           '   20) x; call sub(a,&
      b,&
      c)
   write(10"20) x; call sub(a,&
      b,&
      c)
   "
#endif
contains
   subroutine sub(a,b,c)
   end
   function fun(a,b,c)
      fun=a
   end
end program
subroutine gnikit
 x = p + fun (a,&
  b,&
  c)
100 format(4ha(cd, &
       4hx(bc,&
       i5)
10 call sub1(a,&
  bcd,&
   fun(3.0,&
   4.0,&
      5.0),&
  [6, &
  7, &
  8] &
   )
 call sub1(a,&
  bcd,&
   fun(3.0,&
! comment
   4.0,&
   ! comment
      5.0),&
  [6, &
  7, &
  8] &
   )
contains
 subroutine sub1(a,b,c,d)
    integer d(3)
   end
   function fun(a,b,c)
      fun=a
   end
  end
eof
cat << eof > expect
        program paren
           x = y + &
              fun(a, &
                  abcd ,&
                  z)
           x = y +  &
              z + &
              a; call sub (a, &
!comment
                           b, &
                           c)
           x = y +  a; call sub (a, &
                                 b, &
                                 c)

           print *,'abc'; call sub(a,&
                                   b,&
                                   c)
#if 0
           write(10,'20) x; call sub(a,&
                 b,&
                 c)
           '
           write(10           '   20) x; call sub(a,&
                                                  b,&
                                                  c)
           write(10"20) x; call sub(a,&
                 b,&
                 c)
           "
#endif
        contains
           subroutine sub(a,b,c)
           end
           function fun(a,b,c)
              fun=a
           end
        end program
        subroutine gnikit
           x = p + fun (a,&
                        b,&
                        c)
100        format(4ha(cd, &
              4hx(bc,&
              i5)
10         call sub1(a,&
                     bcd,&
                     fun(3.0,&
                         4.0,&
                         5.0),&
                     [6, &
                      7, &
                      8] &
                     )
           call sub1(a,&
                     bcd,&
                     fun(3.0,&
! comment
                         4.0,&
           ! comment
                         5.0),&
                     [6, &
                      7, &
                      8] &
                     )
        contains
           subroutine sub1(a,b,c,d)
              integer d(3)
           end
           function fun(a,b,c)
              fun=a
           end
        end
eof
../doit "--align_paren --align-paren --align_paren=1 --align-paren=1" "-ifree -I8" " Test026: --align_paren: free"
rc=`expr $rc + $?`

cat << eof > expect
        program paren
           x = y + &
              fun(a, &
                  abcd ,&
                  z)
           x = y +  &
              z + &
              a; call sub (a, &
!comment
                           b, &
                           c)
           x = y +  a; call sub (a, &
                                 b, &
                                 c)

           print *,'abc'; call sub(a,&
                                   b,&
                                   c)
#if 0
           write(10,'20) x; call sub(a,&
                 b,&
                 c)
           '
           write(10           '   20) x; call sub(a,&
                                                  b,&
                                                  c)
           write(10"20) x; call sub(a,&
                 b,&
                 c)
           "
#endif
        contains
           subroutine sub(a,b,c)
           end
           function fun(a,b,c)
              fun=a
           end
        end program
        subroutine gnikit
           x = p + fun (a,&
                        b,&
                        c)
           100 format(4ha(cd, &
              4hx(bc,&
              i5)
           10 call sub1(a,&
                        bcd,&
                        fun(3.0,&
                            4.0,&
                            5.0),&
                        [6, &
                         7, &
                         8] &
                        )
           call sub1(a,&
                     bcd,&
                     fun(3.0,&
! comment
                         4.0,&
           ! comment
                         5.0),&
                     [6, &
                      7, &
                      8] &
                     )
        contains
           subroutine sub1(a,b,c,d)
              integer d(3)
           end
           function fun(a,b,c)
              fun=a
           end
        end
eof
../doit "--align_paren --align-paren --align_paren=1 --align-paren=1 " "-ifree -I8 --label_left=0" " Test027: --align_paren --label_left=0: free"
rc=`expr $rc + $?`

cat << eof > expect
        program paren
           x = y + &
              fun(a, &
              abcd ,&
              z)
           x = y +  &
              z + &
              a; call sub (a, &
!comment
              b, &
              c)
           x = y +  a; call sub (a, &
              b, &
              c)

           print *,'abc'; call sub(a,&
              b,&
              c)
#if 0
           write(10,'20) x; call sub(a,&
              b,&
              c)
           '
           write(10           '   20) x; call sub(a,&
              b,&
              c)
           write(10"20) x; call sub(a,&
              b,&
              c)
           "
#endif
        contains
           subroutine sub(a,b,c)
           end
           function fun(a,b,c)
              fun=a
           end
        end program
        subroutine gnikit
           x = p + fun (a,&
              b,&
              c)
100        format(4ha(cd, &
              4hx(bc,&
              i5)
10         call sub1(a,&
              bcd,&
              fun(3.0,&
              4.0,&
              5.0),&
              [6, &
              7, &
              8] &
              )
           call sub1(a,&
              bcd,&
              fun(3.0,&
! comment
              4.0,&
           ! comment
              5.0),&
              [6, &
              7, &
              8] &
              )
        contains
           subroutine sub1(a,b,c,d)
              integer d(3)
           end
           function fun(a,b,c)
              fun=a
           end
        end
eof
../doit "-kd" "-ifree -I8 -k-" " Test028: -k- -kd: free"
rc=`expr $rc + $?`

cat << eof > expect
        program paren
           x = y + &
              fun(a, &
              abcd ,&
              z)
           x = y +  &
              z + &
              a; call sub (a, &
!comment
              b, &
              c)
           x = y +  a; call sub (a, &
              b, &
              c)

           print *,'abc'; call sub(a,&
              b,&
              c)
#if 0
           write(10,'20) x; call sub(a,&
              b,&
              c)
           '
           write(10           '   20) x; call sub(a,&
              b,&
              c)
           write(10"20) x; call sub(a,&
              b,&
              c)
           "
#endif
        contains
           subroutine sub(a,b,c)
           end
           function fun(a,b,c)
              fun=a
           end
        end program
        subroutine gnikit
           x = p + fun (a,&
              b,&
              c)
100        format(4ha(cd, &
              4hx(bc,&
              i5)
10         call sub1(a,&
              bcd,&
              fun(3.0,&
              4.0,&
              5.0),&
              [6, &
              7, &
              8] &
              )
           call sub1(a,&
              bcd,&
              fun(3.0,&
! comment
              4.0,&
           ! comment
              5.0),&
              [6, &
              7, &
              8] &
              )
        contains
           subroutine sub1(a,b,c,d)
              integer d(3)
           end
           function fun(a,b,c)
              fun=a
           end
        end
eof
../doit "--indent_continuation=default --indent-continuation=default" "-I8 -ifree  -k- " " Test029: -k- --indent_continuation=default: free"
rc=`expr $rc + $?`

cat << eof > expect
program paren
   x = y + &
      fun(a, &
      abcd ,&
      z)
   x = y +  &
      z + &
      a; call sub (a, &
!comment
      b, &
      c)
   x = y +  a; call sub (a, &
      b, &
      c)

   print *,'abc'; call sub(a,&
      b,&
      c)
#if 0
   write(10,'20) x; call sub(a,&
      b,&
      c)
   '
   write(10           '   20) x; call sub(a,&
      b,&
      c)
   write(10"20) x; call sub(a,&
      b,&
      c)
   "
#endif
contains
   subroutine sub(a,b,c)
   end
   function fun(a,b,c)
      fun=a
   end
end program
subroutine gnikit
   x = p + fun (a,&
      b,&
      c)
100 format(4ha(cd, &
      4hx(bc,&
      i5)
10 call sub1(a,&
      bcd,&
      fun(3.0,&
      4.0,&
      5.0),&
      [6, &
      7, &
      8] &
      )
   call sub1(a,&
      bcd,&
      fun(3.0,&
! comment
      4.0,&
   ! comment
      5.0),&
      [6, &
      7, &
      8] &
      )
contains
   subroutine sub1(a,b,c,d)
      integer d(3)
   end
   function fun(a,b,c)
      fun=a
   end
end
eof
../doit "-q" "--safe" " Test030: -k- --indent_continuation=default: free"
rc=`expr $rc + $?`

cat << eof > prog
subroutine remred
 x       = p       +fun        (a,           &
  b,&
  c)
100 format(4habcd             , &
       4hx(bc,&
       i5)

101 format("abcd      "             , &
       'pqr  st    '    ,                  &
       i5)

      call      sub1(a,   "hello              &
   world  "     ,  &
   10)
!$      call      sub1(a,   "hello              &
!$   world  "     ,  &
!$   10)
      call      sub1(a,   'hello              &
   world  '     ,  &
   10)
#if 0
   write(10   '  6)       '  a    b    c  '
   write(10   "  6)       ",  a,    b,    c  ! this    is    not    fortran
#endif
  end
eof
cat << eof > expect
        subroutine remred
           x = p +fun (a, &
              b,&
              c)
100        format(4habcd             , &
              4hx(bc,&
              i5)

101        format("abcd      " , &
              'pqr  st    ' , &
              i5)

           call sub1(a, "hello              &
              world  " , &
              10)
!$         call sub1(a, "hello              &
!$            world  " , &
!$            10)
           call sub1(a, 'hello              &
              world  ' , &
              10)
#if 0
           write(10 ' 6) '  a    b    c  '
           write(10 "  6)       ", a, b, c ! this    is    not    fortran
#endif
        end
eof
../doit "--ws-remred --ws_remred" "-ifree -I8" " Test031: --ws-remred free"
rc=`expr $rc + $?`

cat << eof > prog
       subroutine remred
        x       = p       +fun        (a,           
     1     b,
     2            c)
100   format(4habcd             , 
     1 4hx(bc,
     2 i5)

101    format("abcd      "             , 
     1 'pqr  st    '    ,                  
     2 i5)

       call      sub1(a,   "hello              
     1  world  "     ,
     2  10)
c$       call      sub1(a,   "hello              
!$   1  world  "     ,
c$   2  10)
      call      sub1(a,   'hello              
     1 world  '     ,
     2 10)
#if 0
       write(10   '  6)       '  a    b    c  '
      write(10   "  6)       ",  a,    b,    c  ! this    is    not    fortran
#endif
      end
eof
cat << eof > expect
          subroutine remred
             x = p +fun (a,
     1          b,
     2                 c)
  100        format(4habcd             ,
     1        4hx(bc,
     2        i5)

  101        format("abcd      " ,
     1       'pqr  st    ' ,
     2       i5)

             call sub1(a, "hello
     1  world  " ,
     2        10)
c$           call sub1(a, "hello
!$   1  world  " ,
c$   2       10)
             call sub1(a, 'hello
     1 world  ' ,
     2        10)
#if 0
             write(10 ' 6) '  a    b    c  '
             write(10 "  6)       ", a, b, c ! this    is    not    fortran
#endif
          end
eof
../doit "--ws-remred --ws_remred" "-ifixed -I4" " Test032: --ws-remred fixed"
rc=`expr $rc + $?`

cat << eof > prog
    subroutine mysub
    a = &
    & 3 + &
    & 4 +&
    ! comment
    ! another comment
    5 +&
    & 6
    continue

    !$ x = &
    !$ & 6 + &
    !comment
    !$ & 7 + &
    !$ 8
end
eof
cat << eof > expect
    subroutine mysub
       a = &
       & 3 + &
       & 4 +&
       ! comment
       ! another comment
          5 +&
       & 6
       continue

!$     x = &
!$     & 6 + &
       !comment
!$     & 7 + &
!$        8
    end
eof
../doit "-Ia" "-ifree -Ia" " Test033: defaults for indenting continuation lines"
rc=`expr $rc + $?`
cat << eof > expect
    subroutine mysub
       a = &
          & 3 + &
          & 4 +&
       ! comment
       ! another comment
          5 +&
          & 6
       continue

!$     x = &
!$        & 6 + &
       !comment
!$        & 7 + &
!$        8
    end
eof
../doit "-K --indent_ampersand --indent-ampersand" "-ifree -Ia" " Test034: -K --indent_ampersand --indent-ampersand"
rc=`expr $rc + $?`
cat << eof > expect
    subroutine mysub
       a = &
                & 3 + &
                & 4 +&
       ! comment
       ! another comment
                5 +&
                & 6
       continue

!$     x = &
!$              & 6 + &
       !comment
!$              & 7 + &
!$              8
    end
eof
../doit "-K --indent_ampersand --indent-ampersand" "-ifree -Ia -k9" " Test035: -K --indent_ampersand --indent-ampersand -k9"
rc=`expr $rc + $?`

. ../postlude
exit $rc
# vim: indentexpr=none
