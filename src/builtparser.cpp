/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 30 "../src/parser.y"

#include <iostream>

#include "debug.h"
#include "lexer.h"
#include "line_prep.h"
#include "prop.h"

struct propstruct properties;
bool parselabeling = false;
int  itemnumber = 0;

#line 84 "parser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.hpp"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_MODULE = 3,                     /* MODULE  */
  YYSYMBOL_SUBFUN = 4,                     /* SUBFUN  */
  YYSYMBOL_ENTRY = 5,                      /* ENTRY  */
  YYSYMBOL_SUBMODULE = 6,                  /* SUBMODULE  */
  YYSYMBOL_MODULESUBROUTINE = 7,           /* MODULESUBROUTINE  */
  YYSYMBOL_MODULEFUNCTION = 8,             /* MODULEFUNCTION  */
  YYSYMBOL_END = 9,                        /* END  */
  YYSYMBOL_ENDSUBROUTINE = 10,             /* ENDSUBROUTINE  */
  YYSYMBOL_ENDFUNCTION = 11,               /* ENDFUNCTION  */
  YYSYMBOL_ENDPROGRAM = 12,                /* ENDPROGRAM  */
  YYSYMBOL_ENDMODULE = 13,                 /* ENDMODULE  */
  YYSYMBOL_ENDSUBMODULE = 14,              /* ENDSUBMODULE  */
  YYSYMBOL_IF = 15,                        /* IF  */
  YYSYMBOL_THEN = 16,                      /* THEN  */
  YYSYMBOL_ELSE = 17,                      /* ELSE  */
  YYSYMBOL_ELSEIF = 18,                    /* ELSEIF  */
  YYSYMBOL_ENDIF = 19,                     /* ENDIF  */
  YYSYMBOL_SIMPLEIF = 20,                  /* SIMPLEIF  */
  YYSYMBOL_NWAYIF = 21,                    /* NWAYIF  */
  YYSYMBOL_WHERE = 22,                     /* WHERE  */
  YYSYMBOL_ENDWHERE = 23,                  /* ENDWHERE  */
  YYSYMBOL_FORALL = 24,                    /* FORALL  */
  YYSYMBOL_ENDFORALL = 25,                 /* ENDFORALL  */
  YYSYMBOL_ELSEWHERE = 26,                 /* ELSEWHERE  */
  YYSYMBOL_DO = 27,                        /* DO  */
  YYSYMBOL_DOCOMMA = 28,                   /* DOCOMMA  */
  YYSYMBOL_DOWHILE = 29,                   /* DOWHILE  */
  YYSYMBOL_DOCONCURRENT = 30,              /* DOCONCURRENT  */
  YYSYMBOL_ENDDO = 31,                     /* ENDDO  */
  YYSYMBOL_SELECTCASE = 32,                /* SELECTCASE  */
  YYSYMBOL_SELECTTYPE = 33,                /* SELECTTYPE  */
  YYSYMBOL_ENDSELECT = 34,                 /* ENDSELECT  */
  YYSYMBOL_CASE = 35,                      /* CASE  */
  YYSYMBOL_CASEDEFAULT = 36,               /* CASEDEFAULT  */
  YYSYMBOL_TYPEIS = 37,                    /* TYPEIS  */
  YYSYMBOL_CLASSIS = 38,                   /* CLASSIS  */
  YYSYMBOL_CLASSDEFAULT = 39,              /* CLASSDEFAULT  */
  YYSYMBOL_SELECTRANK = 40,                /* SELECTRANK  */
  YYSYMBOL_RANK = 41,                      /* RANK  */
  YYSYMBOL_RANKDEFAULT = 42,               /* RANKDEFAULT  */
  YYSYMBOL_INTERFACE = 43,                 /* INTERFACE  */
  YYSYMBOL_INTERFACE1 = 44,                /* INTERFACE1  */
  YYSYMBOL_ABSTRACTINTERFACE = 45,         /* ABSTRACTINTERFACE  */
  YYSYMBOL_ENDINTERFACE = 46,              /* ENDINTERFACE  */
  YYSYMBOL_CONTAINS = 47,                  /* CONTAINS  */
  YYSYMBOL_BLOCK = 48,                     /* BLOCK  */
  YYSYMBOL_ENDBLOCK = 49,                  /* ENDBLOCK  */
  YYSYMBOL_BLOCKDATA = 50,                 /* BLOCKDATA  */
  YYSYMBOL_ENDBLOCKDATA = 51,              /* ENDBLOCKDATA  */
  YYSYMBOL_ASSOCIATE = 52,                 /* ASSOCIATE  */
  YYSYMBOL_ENDASSOCIATE = 53,              /* ENDASSOCIATE  */
  YYSYMBOL_CRITICAL = 54,                  /* CRITICAL  */
  YYSYMBOL_ENDCRITICAL = 55,               /* ENDCRITICAL  */
  YYSYMBOL_CHANGETEAM = 56,                /* CHANGETEAM  */
  YYSYMBOL_ENDTEAM = 57,                   /* ENDTEAM  */
  YYSYMBOL_ENUM = 58,                      /* ENUM  */
  YYSYMBOL_ENDENUM = 59,                   /* ENDENUM  */
  YYSYMBOL_ASSIGNMENT = 60,                /* ASSIGNMENT  */
  YYSYMBOL_ASSIGN = 61,                    /* ASSIGN  */
  YYSYMBOL_TO = 62,                        /* TO  */
  YYSYMBOL_GOTO = 63,                      /* GOTO  */
  YYSYMBOL_GOTO_COMPUTED = 64,             /* GOTO_COMPUTED  */
  YYSYMBOL_GOTO_ASSIGNED = 65,             /* GOTO_ASSIGNED  */
  YYSYMBOL_READ = 66,                      /* READ  */
  YYSYMBOL_PRINT = 67,                     /* PRINT  */
  YYSYMBOL_WRITE = 68,                     /* WRITE  */
  YYSYMBOL_IO = 69,                        /* IO  */
  YYSYMBOL_IOLIST = 70,                    /* IOLIST  */
  YYSYMBOL_STARTIO = 71,                   /* STARTIO  */
  YYSYMBOL_LABELIS = 72,                   /* LABELIS  */
  YYSYMBOL_OTHER = 73,                     /* OTHER  */
  YYSYMBOL_MODULEPROCEDURE = 74,           /* MODULEPROCEDURE  */
  YYSYMBOL_PROCEDURE = 75,                 /* PROCEDURE  */
  YYSYMBOL_ENDPROCEDURE = 76,              /* ENDPROCEDURE  */
  YYSYMBOL_TIDENTIFIER = 77,               /* TIDENTIFIER  */
  YYSYMBOL_BLANK = 78,                     /* BLANK  */
  YYSYMBOL_CHAR = 79,                      /* CHAR  */
  YYSYMBOL_FINDFORMAT = 80,                /* FINDFORMAT  */
  YYSYMBOL_UNKNOWN = 81,                   /* UNKNOWN  */
  YYSYMBOL_FREE = 82,                      /* FREE  */
  YYSYMBOL_FIXED = 83,                     /* FIXED  */
  YYSYMBOL_UNSURE = 84,                    /* UNSURE  */
  YYSYMBOL_PROBFREE = 85,                  /* PROBFREE  */
  YYSYMBOL_FINDENTFIX = 86,                /* FINDENTFIX  */
  YYSYMBOL_FIXFINDENTFIX = 87,             /* FIXFINDENTFIX  */
  YYSYMBOL_P_ON = 88,                      /* P_ON  */
  YYSYMBOL_P_OFF = 89,                     /* P_OFF  */
  YYSYMBOL_SCANFIXPRE = 90,                /* SCANFIXPRE  */
  YYSYMBOL_CPP_IF = 91,                    /* CPP_IF  */
  YYSYMBOL_CPP_ENDIF = 92,                 /* CPP_ENDIF  */
  YYSYMBOL_CPP_ELSE = 93,                  /* CPP_ELSE  */
  YYSYMBOL_CPP_ELIF = 94,                  /* CPP_ELIF  */
  YYSYMBOL_CPP = 95,                       /* CPP  */
  YYSYMBOL_COCO_IF = 96,                   /* COCO_IF  */
  YYSYMBOL_COCO_ENDIF = 97,                /* COCO_ENDIF  */
  YYSYMBOL_COCO_ELSE = 98,                 /* COCO_ELSE  */
  YYSYMBOL_COCO_ELIF = 99,                 /* COCO_ELIF  */
  YYSYMBOL_COCO = 100,                     /* COCO  */
  YYSYMBOL_FYPP_IF = 101,                  /* FYPP_IF  */
  YYSYMBOL_FYPP_ENDIF = 102,               /* FYPP_ENDIF  */
  YYSYMBOL_FYPP_ELSE = 103,                /* FYPP_ELSE  */
  YYSYMBOL_FYPP_ELIF = 104,                /* FYPP_ELIF  */
  YYSYMBOL_FYPP = 105,                     /* FYPP  */
  YYSYMBOL_INCLUDE = 106,                  /* INCLUDE  */
  YYSYMBOL_INCLUDE_CPP = 107,              /* INCLUDE_CPP  */
  YYSYMBOL_INCLUDE_CPP_STD = 108,          /* INCLUDE_CPP_STD  */
  YYSYMBOL_INCLUDE_COCO = 109,             /* INCLUDE_COCO  */
  YYSYMBOL_INCLUDE_FYPP = 110,             /* INCLUDE_FYPP  */
  YYSYMBOL_INCFILENAME = 111,              /* INCFILENAME  */
  YYSYMBOL_USE = 112,                      /* USE  */
  YYSYMBOL_SEGMENT = 113,                  /* SEGMENT  */
  YYSYMBOL_ENDSEGMENT = 114,               /* ENDSEGMENT  */
  YYSYMBOL_ESOPE = 115,                    /* ESOPE  */
  YYSYMBOL_IDENTIFIER = 116,               /* IDENTIFIER  */
  YYSYMBOL_CIDENTIFIER = 117,              /* CIDENTIFIER  */
  YYSYMBOL_SKIP = 118,                     /* SKIP  */
  YYSYMBOL_SKIPALL = 119,                  /* SKIPALL  */
  YYSYMBOL_SKIPNOOP = 120,                 /* SKIPNOOP  */
  YYSYMBOL_SKIPNOS = 121,                  /* SKIPNOS  */
  YYSYMBOL_KEYWORD = 122,                  /* KEYWORD  */
  YYSYMBOL_ELEMENTAL = 123,                /* ELEMENTAL  */
  YYSYMBOL_IMPURE = 124,                   /* IMPURE  */
  YYSYMBOL_PURE = 125,                     /* PURE  */
  YYSYMBOL_SIMPLE = 126,                   /* SIMPLE  */
  YYSYMBOL_RECURSIVE = 127,                /* RECURSIVE  */
  YYSYMBOL_NON_RECURSIVE = 128,            /* NON_RECURSIVE  */
  YYSYMBOL_SUBROUTINE = 129,               /* SUBROUTINE  */
  YYSYMBOL_FUNCTION = 130,                 /* FUNCTION  */
  YYSYMBOL_PROGRAM = 131,                  /* PROGRAM  */
  YYSYMBOL_EOL = 132,                      /* EOL  */
  YYSYMBOL_NAMED_LABEL = 133,              /* NAMED_LABEL  */
  YYSYMBOL_STLABEL = 134,                  /* STLABEL  */
  YYSYMBOL_LABEL = 135,                    /* LABEL  */
  YYSYMBOL_LABELS = 136,                   /* LABELS  */
  YYSYMBOL_LABELLIST = 137,                /* LABELLIST  */
  YYSYMBOL_CALL = 138,                     /* CALL  */
  YYSYMBOL_STARTCALL = 139,                /* STARTCALL  */
  YYSYMBOL_CALLLIST = 140,                 /* CALLLIST  */
  YYSYMBOL_TYPE = 141,                     /* TYPE  */
  YYSYMBOL_ENDTYPE = 142,                  /* ENDTYPE  */
  YYSYMBOL_CLASS = 143,                    /* CLASS  */
  YYSYMBOL_BASICTYPE = 144,                /* BASICTYPE  */
  YYSYMBOL_TYPEC = 145,                    /* TYPEC  */
  YYSYMBOL_QSTRING = 146,                  /* QSTRING  */
  YYSYMBOL_HSTRING = 147,                  /* HSTRING  */
  YYSYMBOL_LR = 148,                       /* LR  */
  YYSYMBOL_LRB = 149,                      /* LRB  */
  YYSYMBOL_DOTOPERATOR = 150,              /* DOTOPERATOR  */
  YYSYMBOL_I_NUMBER = 151,                 /* I_NUMBER  */
  YYSYMBOL_UNCLASSIFIED = 152,             /* UNCLASSIFIED  */
  YYSYMBOL_ERROR = 153,                    /* ERROR  */
  YYSYMBOL_OMP = 154,                      /* OMP  */
  YYSYMBOL_SCANOMPFIXED = 155,             /* SCANOMPFIXED  */
  YYSYMBOL_SCANOMPFREE = 156,              /* SCANOMPFREE  */
  YYSYMBOL_DEF = 157,                      /* DEF  */
  YYSYMBOL_EXEC = 158,                     /* EXEC  */
  YYSYMBOL_ENDDEF = 159,                   /* ENDDEF  */
  YYSYMBOL_NONE = 160,                     /* NONE  */
  YYSYMBOL_LAST_TOKEN = 161,               /* LAST_TOKEN  */
  YYSYMBOL_162_ = 162,                     /* '%'  */
  YYSYMBOL_163_ = 163,                     /* ','  */
  YYSYMBOL_164_ = 164,                     /* '*'  */
  YYSYMBOL_165_ = 165,                     /* '='  */
  YYSYMBOL_166_ = 166,                     /* ':'  */
  YYSYMBOL_YYACCEPT = 167,                 /* $accept  */
  YYSYMBOL_lline = 168,                    /* lline  */
  YYSYMBOL_labels = 169,                   /* labels  */
  YYSYMBOL_line = 170,                     /* line  */
  YYSYMBOL_blank = 171,                    /* blank  */
  YYSYMBOL_stlabel = 172,                  /* stlabel  */
  YYSYMBOL_named_label = 173,              /* named_label  */
  YYSYMBOL_module = 174,                   /* module  */
  YYSYMBOL_use = 175,                      /* use  */
  YYSYMBOL_include = 176,                  /* include  */
  YYSYMBOL_identifiers = 177,              /* identifiers  */
  YYSYMBOL_abstractinterface = 178,        /* abstractinterface  */
  YYSYMBOL_contains = 179,                 /* contains  */
  YYSYMBOL_interface = 180,                /* interface  */
  YYSYMBOL_moduleprocedure = 181,          /* moduleprocedure  */
  YYSYMBOL_procedure = 182,                /* procedure  */
  YYSYMBOL_program_stmt = 183,             /* program_stmt  */
  YYSYMBOL_subroutine_stmt = 184,          /* subroutine_stmt  */
  YYSYMBOL_subroutine = 185,               /* subroutine  */
  YYSYMBOL_subroutine_spec = 186,          /* subroutine_spec  */
  YYSYMBOL_subroutinename = 187,           /* subroutinename  */
  YYSYMBOL_subroutineprefix = 188,         /* subroutineprefix  */
  YYSYMBOL_subroutineprefix_spec = 189,    /* subroutineprefix_spec  */
  YYSYMBOL_function_stmt = 190,            /* function_stmt  */
  YYSYMBOL_function = 191,                 /* function  */
  YYSYMBOL_function_spec = 192,            /* function_spec  */
  YYSYMBOL_functionname = 193,             /* functionname  */
  YYSYMBOL_submodule = 194,                /* submodule  */
  YYSYMBOL_intrinsic_type_spec = 195,      /* intrinsic_type_spec  */
  YYSYMBOL_kind_selector = 196,            /* kind_selector  */
  YYSYMBOL_entry = 197,                    /* entry  */
  YYSYMBOL_endassociate = 198,             /* endassociate  */
  YYSYMBOL_endblock = 199,                 /* endblock  */
  YYSYMBOL_endblockdata = 200,             /* endblockdata  */
  YYSYMBOL_endcritical = 201,              /* endcritical  */
  YYSYMBOL_enddo = 202,                    /* enddo  */
  YYSYMBOL_endenum = 203,                  /* endenum  */
  YYSYMBOL_endforall = 204,                /* endforall  */
  YYSYMBOL_endfunction = 205,              /* endfunction  */
  YYSYMBOL_endif = 206,                    /* endif  */
  YYSYMBOL_endinterface = 207,             /* endinterface  */
  YYSYMBOL_endmodule = 208,                /* endmodule  */
  YYSYMBOL_endprocedure = 209,             /* endprocedure  */
  YYSYMBOL_endprogram = 210,               /* endprogram  */
  YYSYMBOL_endselect = 211,                /* endselect  */
  YYSYMBOL_endsubmodule = 212,             /* endsubmodule  */
  YYSYMBOL_endsubroutine = 213,            /* endsubroutine  */
  YYSYMBOL_endteam = 214,                  /* endteam  */
  YYSYMBOL_endtype = 215,                  /* endtype  */
  YYSYMBOL_endsegment = 216,               /* endsegment  */
  YYSYMBOL_endwhere = 217,                 /* endwhere  */
  YYSYMBOL_simple_end = 218,               /* simple_end  */
  YYSYMBOL_gidentifier = 219,              /* gidentifier  */
  YYSYMBOL_assignment = 220,               /* assignment  */
  YYSYMBOL_assign = 221,                   /* assign  */
  YYSYMBOL_else = 222,                     /* else  */
  YYSYMBOL_elseif = 223,                   /* elseif  */
  YYSYMBOL_elsewhere = 224,                /* elsewhere  */
  YYSYMBOL_if_construct = 225,             /* if_construct  */
  YYSYMBOL_if = 226,                       /* if  */
  YYSYMBOL_nwayif = 227,                   /* nwayif  */
  YYSYMBOL_where_construct = 228,          /* where_construct  */
  YYSYMBOL_forall_construct = 229,         /* forall_construct  */
  YYSYMBOL_do_construct = 230,             /* do_construct  */
  YYSYMBOL_do = 231,                       /* do  */
  YYSYMBOL_docomma = 232,                  /* docomma  */
  YYSYMBOL_goto = 233,                     /* goto  */
  YYSYMBOL_goto_computed = 234,            /* goto_computed  */
  YYSYMBOL_goto_assigned = 235,            /* goto_assigned  */
  YYSYMBOL_goto_prefix = 236,              /* goto_prefix  */
  YYSYMBOL_read = 237,                     /* read  */
  YYSYMBOL_print = 238,                    /* print  */
  YYSYMBOL_io = 239,                       /* io  */
  YYSYMBOL_iolist = 240,                   /* iolist  */
  YYSYMBOL_itemlist = 241,                 /* itemlist  */
  YYSYMBOL_item = 242,                     /* item  */
  YYSYMBOL_243_1 = 243,                    /* $@1  */
  YYSYMBOL_244_2 = 244,                    /* $@2  */
  YYSYMBOL_245_3 = 245,                    /* $@3  */
  YYSYMBOL_otheritem = 246,                /* otheritem  */
  YYSYMBOL_labellist = 247,                /* labellist  */
  YYSYMBOL_selectcase = 248,               /* selectcase  */
  YYSYMBOL_selectrank = 249,               /* selectrank  */
  YYSYMBOL_selecttype = 250,               /* selecttype  */
  YYSYMBOL_call = 251,                     /* call  */
  YYSYMBOL_calllist = 252,                 /* calllist  */
  YYSYMBOL_clist = 253,                    /* clist  */
  YYSYMBOL_citem = 254,                    /* citem  */
  YYSYMBOL_case = 255,                     /* case  */
  YYSYMBOL_casedefault = 256,              /* casedefault  */
  YYSYMBOL_rank = 257,                     /* rank  */
  YYSYMBOL_rankdefault = 258,              /* rankdefault  */
  YYSYMBOL_classdefault = 259,             /* classdefault  */
  YYSYMBOL_classis = 260,                  /* classis  */
  YYSYMBOL_typeis = 261,                   /* typeis  */
  YYSYMBOL_changeteam = 262,               /* changeteam  */
  YYSYMBOL_block = 263,                    /* block  */
  YYSYMBOL_blockdata = 264,                /* blockdata  */
  YYSYMBOL_associate = 265,                /* associate  */
  YYSYMBOL_critical = 266,                 /* critical  */
  YYSYMBOL_enum = 267,                     /* enum  */
  YYSYMBOL_type = 268,                     /* type  */
  YYSYMBOL_type1 = 269,                    /* type1  */
  YYSYMBOL_segment = 270,                  /* segment  */
  YYSYMBOL_lvalue = 271,                   /* lvalue  */
  YYSYMBOL_construct_name = 272,           /* construct_name  */
  YYSYMBOL_lr_construct_name = 273,        /* lr_construct_name  */
  YYSYMBOL_skipall = 274,                  /* skipall  */
  YYSYMBOL_skipnoop = 275,                 /* skipnoop  */
  YYSYMBOL_skipnos = 276,                  /* skipnos  */
  YYSYMBOL_enable_identifier = 277,        /* enable_identifier  */
  YYSYMBOL_enable_cidentifier = 278,       /* enable_cidentifier  */
  YYSYMBOL_enable_skip = 279,              /* enable_skip  */
  YYSYMBOL_enable_skipall = 280,           /* enable_skipall  */
  YYSYMBOL_enable_skipnoop = 281,          /* enable_skipnoop  */
  YYSYMBOL_enable_skipnos = 282,           /* enable_skipnos  */
  YYSYMBOL_getname = 283,                  /* getname  */
  YYSYMBOL_getstlabel = 284,               /* getstlabel  */
  YYSYMBOL_getlabel = 285,                 /* getlabel  */
  YYSYMBOL_getlabel1 = 286,                /* getlabel1  */
  YYSYMBOL_getlabel2 = 287,                /* getlabel2  */
  YYSYMBOL_getstring = 288,                /* getstring  */
  YYSYMBOL_getlr = 289,                    /* getlr  */
  YYSYMBOL_empty = 290                     /* empty  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  10
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   575

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  167
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  124
/* YYNRULES -- Number of rules.  */
#define YYNRULES  259
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  487

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   416


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,   162,     2,     2,
       2,     2,   164,     2,   163,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   166,     2,
       2,   165,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    97,    97,    99,   100,   101,   102,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   180,   182,   184,   186,   188,   190,   192,   193,
     195,   197,   199,   200,   203,   205,   219,   220,   222,   224,
     226,   227,   229,   231,   232,   234,   235,   236,   237,   238,
     239,   240,   241,   242,   244,   246,   248,   249,   251,   253,
     255,   256,   258,   259,   260,   262,   264,   266,   268,   270,
     272,   274,   276,   278,   279,   281,   283,   285,   287,   288,
     290,   291,   293,   295,   297,   298,   300,   302,   304,   306,
     308,   310,   311,   313,   315,   316,   319,   321,   323,   326,
     329,   332,   333,   336,   339,   342,   343,   344,   345,   346,
     347,   351,   353,   356,   357,   360,   361,   363,   364,   366,
     369,   370,   373,   374,   377,   378,   380,   382,   383,   384,
     385,   387,   387,   388,   388,   389,   389,   390,   392,   393,
     396,   397,   398,   401,   403,   405,   408,   409,   410,   412,
     414,   415,   416,   418,   419,   422,   424,   427,   429,   432,
     435,   437,   440,   442,   444,   446,   448,   449,   451,   454,
     455,   456,   457,   460,   463,   464,   465,   467,   468,   470,
     471,   472,   473,   476,   478,   480,   482,   484,   486,   488,
     490,   492,   494,   496,   498,   501,   507,   511,   513,   516
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "MODULE", "SUBFUN",
  "ENTRY", "SUBMODULE", "MODULESUBROUTINE", "MODULEFUNCTION", "END",
  "ENDSUBROUTINE", "ENDFUNCTION", "ENDPROGRAM", "ENDMODULE",
  "ENDSUBMODULE", "IF", "THEN", "ELSE", "ELSEIF", "ENDIF", "SIMPLEIF",
  "NWAYIF", "WHERE", "ENDWHERE", "FORALL", "ENDFORALL", "ELSEWHERE", "DO",
  "DOCOMMA", "DOWHILE", "DOCONCURRENT", "ENDDO", "SELECTCASE",
  "SELECTTYPE", "ENDSELECT", "CASE", "CASEDEFAULT", "TYPEIS", "CLASSIS",
  "CLASSDEFAULT", "SELECTRANK", "RANK", "RANKDEFAULT", "INTERFACE",
  "INTERFACE1", "ABSTRACTINTERFACE", "ENDINTERFACE", "CONTAINS", "BLOCK",
  "ENDBLOCK", "BLOCKDATA", "ENDBLOCKDATA", "ASSOCIATE", "ENDASSOCIATE",
  "CRITICAL", "ENDCRITICAL", "CHANGETEAM", "ENDTEAM", "ENUM", "ENDENUM",
  "ASSIGNMENT", "ASSIGN", "TO", "GOTO", "GOTO_COMPUTED", "GOTO_ASSIGNED",
  "READ", "PRINT", "WRITE", "IO", "IOLIST", "STARTIO", "LABELIS", "OTHER",
  "MODULEPROCEDURE", "PROCEDURE", "ENDPROCEDURE", "TIDENTIFIER", "BLANK",
  "CHAR", "FINDFORMAT", "UNKNOWN", "FREE", "FIXED", "UNSURE", "PROBFREE",
  "FINDENTFIX", "FIXFINDENTFIX", "P_ON", "P_OFF", "SCANFIXPRE", "CPP_IF",
  "CPP_ENDIF", "CPP_ELSE", "CPP_ELIF", "CPP", "COCO_IF", "COCO_ENDIF",
  "COCO_ELSE", "COCO_ELIF", "COCO", "FYPP_IF", "FYPP_ENDIF", "FYPP_ELSE",
  "FYPP_ELIF", "FYPP", "INCLUDE", "INCLUDE_CPP", "INCLUDE_CPP_STD",
  "INCLUDE_COCO", "INCLUDE_FYPP", "INCFILENAME", "USE", "SEGMENT",
  "ENDSEGMENT", "ESOPE", "IDENTIFIER", "CIDENTIFIER", "SKIP", "SKIPALL",
  "SKIPNOOP", "SKIPNOS", "KEYWORD", "ELEMENTAL", "IMPURE", "PURE",
  "SIMPLE", "RECURSIVE", "NON_RECURSIVE", "SUBROUTINE", "FUNCTION",
  "PROGRAM", "EOL", "NAMED_LABEL", "STLABEL", "LABEL", "LABELS",
  "LABELLIST", "CALL", "STARTCALL", "CALLLIST", "TYPE", "ENDTYPE", "CLASS",
  "BASICTYPE", "TYPEC", "QSTRING", "HSTRING", "LR", "LRB", "DOTOPERATOR",
  "I_NUMBER", "UNCLASSIFIED", "ERROR", "OMP", "SCANOMPFIXED",
  "SCANOMPFREE", "DEF", "EXEC", "ENDDEF", "NONE", "LAST_TOKEN", "'%'",
  "','", "'*'", "'='", "':'", "$accept", "lline", "labels", "line",
  "blank", "stlabel", "named_label", "module", "use", "include",
  "identifiers", "abstractinterface", "contains", "interface",
  "moduleprocedure", "procedure", "program_stmt", "subroutine_stmt",
  "subroutine", "subroutine_spec", "subroutinename", "subroutineprefix",
  "subroutineprefix_spec", "function_stmt", "function", "function_spec",
  "functionname", "submodule", "intrinsic_type_spec", "kind_selector",
  "entry", "endassociate", "endblock", "endblockdata", "endcritical",
  "enddo", "endenum", "endforall", "endfunction", "endif", "endinterface",
  "endmodule", "endprocedure", "endprogram", "endselect", "endsubmodule",
  "endsubroutine", "endteam", "endtype", "endsegment", "endwhere",
  "simple_end", "gidentifier", "assignment", "assign", "else", "elseif",
  "elsewhere", "if_construct", "if", "nwayif", "where_construct",
  "forall_construct", "do_construct", "do", "docomma", "goto",
  "goto_computed", "goto_assigned", "goto_prefix", "read", "print", "io",
  "iolist", "itemlist", "item", "$@1", "$@2", "$@3", "otheritem",
  "labellist", "selectcase", "selectrank", "selecttype", "call",
  "calllist", "clist", "citem", "case", "casedefault", "rank",
  "rankdefault", "classdefault", "classis", "typeis", "changeteam",
  "block", "blockdata", "associate", "critical", "enum", "type", "type1",
  "segment", "lvalue", "construct_name", "lr_construct_name", "skipall",
  "skipnoop", "skipnos", "enable_identifier", "enable_cidentifier",
  "enable_skip", "enable_skipall", "enable_skipnoop", "enable_skipnos",
  "getname", "getstlabel", "getlabel", "getlabel1", "getlabel2",
  "getstring", "getlr", "empty", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-402)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-248)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -7,  -402,  -402,    27,   433,  -101,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -117,   -94,  -402,  -402,  -402,  -402,  -402,
    -103,  -402,   -84,  -402,   -61,  -402,   -59,  -402,  -402,  -402,
    -402,   -39,   -23,  -402,   -19,   -18,  -402,   -16,  -402,   -14,
     -13,  -402,   -10,     4,  -402,  -402,  -402,  -402,  -402,   -49,
       8,  -402,  -402,  -402,     7,  -402,  -108,  -402,     9,  -402,
     -22,    24,    10,    11,    16,    22,   -58,  -402,  -402,  -402,
    -402,  -402,    13,  -402,  -402,    39,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,    21,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
      25,  -402,  -402,  -402,  -402,  -402,  -402,    31,  -402,  -402,
    -402,  -402,   -65,   -67,  -402,  -402,  -402,   -12,  -402,  -402,
    -402,  -402,  -119,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -105,  -402,   -51,    70,  -402,  -402,    56,    59,
    -402,  -402,  -106,    61,   -98,   -96,    46,    47,    -8,    48,
     165,    50,    52,    53,    57,    58,    62,  -107,  -402,  -402,
      63,    64,    65,    66,    67,    68,  -402,    69,  -402,  -402,
      72,    74,  -402,    75,  -402,    83,    71,  -402,  -402,  -402,
    -402,    76,    77,    78,    79,    81,  -402,    82,    84,    85,
      86,  -402,  -402,  -402,  -402,  -402,  -402,   113,   118,  -402,
    -116,  -402,  -402,    99,   103,   -95,  -402,   104,   105,  -402,
     -77,  -402,   106,   -30,  -402,  -402,    91,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,    45,   -63,    80,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,    88,  -402,    54,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,    89,  -402,    90,  -402,  -402,    60,   -67,
    -402,  -402,  -402,  -402,  -402,  -402,    92,  -402,  -402,  -402,
      95,  -402,    97,  -402,  -402,    98,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,   116,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,   107,  -402,   108,   110,  -402,  -402,
     111,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,   176,  -402,  -402,  -402,   112,   118,
    -402,  -402,  -402,   -58,  -402,    87,  -402,  -402,   114,   123,
    -402,   124,  -402,  -402,   115,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,   -32,  -402,  -402,  -402,   129,   119,   142,  -402,
    -402,  -402,  -402,  -402,   127,  -402,  -402,  -402,    94,  -402,
     145,  -402,  -402,   144,   130,  -402,   150,  -402,  -402,  -402,
    -402,  -402,   109,   136,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,   153,
     138,   139,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,   155,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,   120,  -402,  -402,  -402,  -402,   125,  -402,  -402,
    -402,   -67,  -402,  -402,  -402,  -402,   156,   160,  -402,  -402,
    -402,  -402,   161,  -402,  -402,  -402,  -402,   149,  -402,  -102,
    -402,  -402,  -402,  -402,   158,  -402,  -402,  -109,  -402,  -402,
    -402,   131,  -402,  -402,  -402,   152,  -402
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
     259,   252,   253,     0,   259,     4,     5,     3,    84,    83,
       1,   246,   246,     0,     0,   246,   246,   246,   246,   246,
       0,   246,     0,   246,     0,   246,     0,   246,   246,   246,
     246,     0,     0,   246,     0,     0,   246,     0,   246,     0,
       0,   246,     0,     0,   246,   249,   246,   249,   249,     0,
       0,   246,   246,   246,     0,   246,     0,   246,     0,   246,
       0,     0,     0,     0,     0,     0,     0,   246,   246,   246,
     152,    82,     0,   246,   246,     0,   151,   246,   254,   246,
     259,   246,   246,     2,    12,    60,    78,    57,     8,    24,
      58,    61,    65,    64,    73,     0,    74,    75,    49,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    81,    48,    72,
     234,    10,     9,    27,    28,    29,    56,    55,    62,    79,
      51,    26,     0,     0,    52,    54,    53,     0,    66,    63,
      67,    68,    59,    69,    70,    71,    15,    16,    17,    18,
      19,    20,    22,    23,    77,    21,    13,    14,    11,    25,
      50,    76,     0,    80,     0,     0,   103,     6,     0,     0,
     258,   150,     0,   259,     0,     0,     0,     0,   160,     0,
       0,     0,     0,     0,     0,     0,     0,   259,   171,   172,
       0,     0,     0,     0,     0,     0,   246,     0,   246,   246,
       0,     0,   246,     0,    92,     0,     0,    90,   136,    91,
     223,     0,     0,     0,     0,     0,   226,     0,     0,     0,
       0,   249,   131,   254,   254,   254,   258,   193,   198,   191,
     186,   187,   197,     0,     0,     0,   257,     0,     0,   148,
       0,   200,     0,   209,   210,   232,     0,   101,   117,   105,
     106,   107,   108,   109,   110,   100,   116,     0,   120,     0,
     246,    99,   104,   246,   115,   111,   235,     0,   155,   247,
     174,   176,   178,   181,   183,   185,   208,   168,   254,   249,
     249,   258,   254,     0,   202,     0,   252,   251,     0,     0,
     250,   179,   252,   249,   246,   144,     0,   252,   237,   133,
       0,   140,     0,   137,   143,     0,   254,   156,   246,   135,
     163,   149,   164,   132,   158,   252,   240,   239,   169,   170,
     130,   203,   205,   142,     0,   216,     0,     0,   219,   204,
       0,   218,   243,   252,   127,   224,   128,   225,   126,   227,
     129,   222,   146,   228,     0,   249,   249,   249,     0,     0,
     199,   255,   190,   189,    88,   252,   252,   138,     0,     0,
     252,     0,   252,    97,   258,   214,   212,   256,   211,   147,
     113,   124,     0,   121,   112,   248,     0,     0,     0,   160,
     249,   166,   167,   249,     0,   258,   254,   249,     0,   245,
       0,   236,   153,     0,     0,   125,     0,   145,   238,   134,
     141,   159,     0,     0,   241,   252,   215,   221,   220,   217,
     249,   246,   180,   182,   184,   254,   196,   192,   188,     0,
       0,     0,   139,    87,   249,   233,   248,   258,   249,   213,
     123,   122,     0,   252,   248,   252,   165,   175,   173,   249,
     201,   229,     0,   252,   244,    85,   252,     0,   157,   242,
      93,     0,   194,    89,    94,    95,     0,     0,   249,   207,
      98,   102,     0,   118,   177,   246,   249,     0,   254,     0,
      86,    96,   206,   114,     0,   230,   119,     0,   154,   252,
     161,     0,   249,   254,   231,     0,   162
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -402,  -402,  -402,  -402,  -402,  -402,   280,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,   -15,  -402,   283,  -402,  -402,  -402,  -402,   284,
    -402,  -402,  -402,  -402,  -402,  -402,   285,   286,   287,  -402,
     288,   289,   290,  -402,  -402,   -57,  -402,  -402,  -402,  -221,
    -402,  -402,  -402,  -402,   291,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,  -402,
    -402,  -402,  -402,  -402,  -283,    55,   238,   -45,  -402,  -402,
     -11,  -402,  -401,  -126,  -402,  -402,  -202,  -402,  -203,  -402,
    -402,  -402,  -222,     1
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,     3,     4,    83,    84,     5,     6,    85,    86,    87,
     355,    88,    89,    90,    91,    92,    93,    94,   260,   261,
     375,    95,   262,    96,   263,   264,   377,    97,   265,   373,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   268,   123,   124,   125,   126,   269,
     128,   129,   130,   131,   132,   133,   270,   271,   272,   137,
     273,   274,   275,   141,   230,   231,   351,   348,   349,   232,
     142,   143,   144,   145,   276,   147,   243,   368,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   172,   186,   204,   392,   388,
     173,   165,   432,   205,   393,   389,     8,     9,   241,   417,
     429,   359,   294,     7
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     168,   169,   207,   208,   347,   166,   391,   350,   305,   315,
      70,   286,    70,   284,   227,   228,   352,   187,   188,   189,
     344,   345,   346,   480,   216,   457,   295,    10,   247,   248,
     478,   170,     1,   462,   299,   206,   301,   357,   171,   362,
     217,   316,   296,   365,   285,   178,   267,   353,   187,    76,
     300,    76,   302,   358,   481,   363,   233,   234,   287,   383,
     289,   288,   237,   238,   180,    70,   240,   277,   242,   267,
     245,   174,   175,   176,   177,   380,   179,   229,   181,   384,
     183,   244,   185,   209,   387,   371,   278,   182,   192,   184,
     394,   195,    62,   197,  -247,   398,   200,    63,    64,   203,
      65,   372,   366,   402,    76,   367,   211,   212,   213,   190,
     215,   289,   218,   404,   290,    62,   430,   279,   280,   431,
      63,    64,   283,    65,   235,   191,     1,     2,   416,   193,
     194,   410,   196,   291,   198,   199,   281,   246,   201,   282,
     210,   221,   428,   306,   249,   250,   251,   252,   253,   254,
     255,   256,   202,   420,   421,   214,   222,   219,   424,   236,
     426,   223,   224,   439,   257,   258,   259,   225,   469,    79,
     226,   239,   292,   266,   298,   293,   343,   297,   303,   304,
     307,   308,   309,   440,   310,   311,  -195,   333,   317,   312,
     313,   228,    79,   370,   314,   318,   319,   320,   321,   322,
     323,   325,   332,   449,   328,   458,   329,   331,   334,   335,
     336,   337,   452,   338,   339,   354,   340,   341,   342,   356,
     360,   361,   364,   369,   397,   386,   390,   399,   374,   400,
     401,   461,   405,   463,   381,   382,   379,   385,   411,   406,
     407,   466,   408,   409,   467,   433,   422,   415,   395,   376,
     419,   324,   378,   326,   327,   423,   425,   330,   435,   438,
     442,   443,   445,   427,   444,   477,   446,   434,   448,   453,
     454,   455,   447,   460,   479,   470,   468,   482,   471,   473,
     485,   476,   483,   396,   486,   167,   465,   122,   127,   134,
     135,   136,   138,   139,   140,   146,   418,   220,   456,     0,
     412,   413,   414,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   436,     0,     0,   437,     0,
       0,     0,   441,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   403,     0,   450,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   459,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   464,     0,     0,     0,     0,     0,
     451,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   472,     0,     0,     0,     0,     0,     0,
       0,   475,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    -7,     0,     0,    11,   484,    12,    13,
       0,     0,    14,    15,    16,    17,    18,    19,    20,     0,
      21,    22,    23,     0,   474,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,     0,    62,     0,  -247,     0,     0,    63,
      64,     0,    65,     0,    66,     0,     0,    67,    68,    69,
      70,    71,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    72,
       0,     0,     0,     0,     0,    73,    74,    75,     0,    76,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    77,     0,     0,     0,    78,     0,
       0,    79,    80,     0,    81,    82
};

static const yytype_int16 yycheck[] =
{
      11,    12,    47,    48,   226,     4,   289,   228,    16,   116,
      77,   116,    77,   132,    72,    73,   132,    28,    29,    30,
     223,   224,   225,   132,   132,   426,   132,     0,     7,     8,
     132,   148,   133,   434,   132,    46,   132,   132,   132,   116,
     148,   148,   148,    73,   163,   148,    15,   163,    59,   116,
     148,   116,   148,   148,   163,   132,    67,    68,   163,   281,
     162,   166,    73,    74,   148,    77,    77,   132,    79,    15,
      81,    16,    17,    18,    19,   278,    21,   135,    23,   282,
      25,    80,    27,   132,   286,   148,   151,   148,    33,   148,
     292,    36,    61,    38,    63,   297,    41,    66,    67,    44,
      69,   164,   132,   306,   116,   135,    51,    52,    53,   148,
      55,   162,    57,   315,   165,    61,   148,   132,   133,   151,
      66,    67,   137,    69,    69,   148,   133,   134,   349,   148,
     148,   333,   148,    63,   148,   148,   148,    82,   148,   151,
     132,   163,   364,   151,   123,   124,   125,   126,   127,   128,
     129,   130,   148,   355,   356,   148,   132,   148,   360,   146,
     362,   151,   151,   385,   143,   144,   145,   151,   451,   138,
     148,   132,   116,   148,   173,   116,   221,   116,   132,   132,
     132,    16,   132,   386,   132,   132,    73,   116,   187,   132,
     132,    73,   138,   148,   132,   132,   132,   132,   132,   132,
     132,   132,   119,   405,   132,   427,   132,   132,   132,   132,
     132,   132,   415,   132,   132,   116,   132,   132,   132,   116,
     116,   116,   116,   132,   132,   135,   166,   132,   148,   132,
     132,   433,   116,   435,   279,   280,   148,   148,    62,   132,
     132,   443,   132,   132,   446,   116,   132,   135,   293,   260,
     163,   196,   263,   198,   199,   132,   132,   202,   116,   132,
     166,   116,   132,   148,   120,   468,   116,   148,   132,   116,
     132,   132,   163,   118,   116,   119,   151,   479,   118,   118,
     483,   132,   151,   294,   132,     5,   166,     4,     4,     4,
       4,     4,     4,     4,     4,     4,   353,    59,   424,    -1,
     345,   346,   347,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   380,    -1,    -1,   383,    -1,
      -1,    -1,   387,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   308,    -1,   410,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   428,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   439,    -1,    -1,    -1,    -1,    -1,
     411,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   458,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   466,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     0,    -1,    -1,     3,   482,     5,     6,
      -1,    -1,     9,    10,    11,    12,    13,    14,    15,    -1,
      17,    18,    19,    -1,   465,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    -1,    61,    -1,    63,    -1,    -1,    66,
      67,    -1,    69,    -1,    71,    -1,    -1,    74,    75,    76,
      77,    78,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   106,
      -1,    -1,    -1,    -1,    -1,   112,   113,   114,    -1,   116,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   131,    -1,    -1,    -1,   135,    -1,
      -1,   138,   139,    -1,   141,   142
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,   133,   134,   168,   169,   172,   173,   290,   283,   284,
       0,     3,     5,     6,     9,    10,    11,    12,    13,    14,
      15,    17,    18,    19,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    61,    66,    67,    69,    71,    74,    75,    76,
      77,    78,   106,   112,   113,   114,   116,   131,   135,   138,
     139,   141,   142,   170,   171,   174,   175,   176,   178,   179,
     180,   181,   182,   183,   184,   188,   190,   194,   197,   198,
     199,   200,   201,   202,   203,   204,   205,   206,   207,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,   234,   235,   236,   237,   238,
     239,   240,   247,   248,   249,   250,   251,   252,   255,   256,
     257,   258,   259,   260,   261,   262,   263,   264,   265,   266,
     267,   268,   269,   270,   271,   278,   290,   173,   277,   277,
     148,   132,   272,   277,   272,   272,   272,   272,   148,   272,
     148,   272,   148,   272,   148,   272,   273,   277,   277,   277,
     148,   148,   272,   148,   148,   272,   148,   272,   148,   148,
     272,   148,   148,   272,   274,   280,   277,   274,   274,   132,
     132,   272,   272,   272,   148,   272,   132,   148,   272,   148,
     273,   163,   132,   151,   151,   151,   148,    72,    73,   135,
     241,   242,   246,   277,   277,   272,   146,   277,   277,   132,
     277,   285,   277,   253,   290,   277,   272,     7,     8,   123,
     124,   125,   126,   127,   128,   129,   130,   143,   144,   145,
     185,   186,   189,   191,   192,   195,   148,    15,   221,   226,
     233,   234,   235,   237,   238,   239,   251,   132,   151,   219,
     219,   148,   151,   219,   132,   163,   116,   163,   166,   162,
     165,    63,   116,   116,   289,   132,   148,   116,   290,   132,
     148,   132,   148,   132,   132,    16,   151,   132,    16,   132,
     132,   132,   132,   132,   132,   116,   148,   290,   132,   132,
     132,   132,   132,   132,   272,   132,   272,   272,   132,   132,
     272,   132,   119,   116,   132,   132,   132,   132,   132,   132,
     132,   132,   132,   274,   285,   285,   285,   289,   244,   245,
     246,   243,   132,   163,   116,   177,   116,   132,   148,   288,
     116,   116,   116,   132,   116,    73,   132,   135,   254,   132,
     148,   148,   164,   196,   148,   187,   277,   193,   277,   148,
     285,   274,   274,   289,   285,   148,   135,   283,   276,   282,
     166,   271,   275,   281,   283,   274,   277,   132,   283,   132,
     132,   132,   285,   272,   283,   116,   132,   132,   132,   132,
     283,    62,   274,   274,   274,   135,   246,   286,   242,   163,
     283,   283,   132,   132,   283,   132,   283,   148,   289,   287,
     148,   151,   279,   116,   148,   116,   274,   274,   132,   289,
     285,   274,   166,   116,   120,   132,   116,   163,   132,   283,
     274,   277,   285,   116,   132,   132,   280,   279,   289,   274,
     118,   283,   279,   283,   274,   166,   283,   283,   151,   271,
     119,   118,   274,   118,   277,   274,   132,   285,   132,   116,
     132,   163,   283,   151,   274,   285,   132
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   167,   168,   169,   169,   169,   169,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   170,   170,   170,   170,   170,   170,   170,   170,
     170,   170,   171,   172,   173,   174,   175,   176,   177,   177,
     178,   179,   180,   180,   181,   182,   183,   183,   184,   185,
     186,   186,   187,   188,   188,   189,   189,   189,   189,   189,
     189,   189,   189,   189,   190,   191,   192,   192,   193,   194,
     195,   195,   196,   196,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   205,   206,   207,   208,   209,   209,
     210,   210,   211,   212,   213,   213,   214,   215,   216,   217,
     218,   219,   219,   220,   221,   221,   222,   223,   224,   225,
     226,   227,   227,   228,   229,   230,   230,   230,   230,   230,
     230,   231,   232,   233,   233,   234,   234,   235,   235,   236,
     237,   237,   238,   238,   239,   239,   240,   241,   241,   241,
     241,   243,   242,   244,   242,   245,   242,   242,   246,   246,
     247,   247,   247,   248,   249,   250,   251,   251,   251,   252,
     253,   253,   253,   254,   254,   255,   256,   257,   258,   259,
     260,   261,   262,   263,   264,   265,   266,   266,   267,   268,
     268,   268,   269,   270,   271,   271,   271,   272,   272,   273,
     273,   273,   273,   274,   275,   276,   277,   278,   279,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     1,     1,     1,     2,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     2,     5,     6,     4,     1,     3,
       2,     2,     2,     5,     5,     5,     6,     3,     5,     1,
       1,     1,     3,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     2,     2,     6,     1,     1,     1,     3,     7,
       1,     2,     2,     2,     1,     4,     3,     3,     3,     3,
       3,     2,     3,     3,     4,     3,     2,     3,     3,     4,
       3,     4,     3,     3,     3,     4,     3,     3,     2,     3,
       2,     1,     1,     3,     7,     2,     3,     5,     3,     4,
       2,     8,    11,     3,     3,     4,     3,     3,     2,     3,
       3,     2,     2,     4,     2,     4,     2,     5,     2,     2,
       4,     2,     4,     2,     4,     2,     2,     1,     3,     2,
       2,     0,     3,     0,     4,     0,     3,     1,     1,     2,
       2,     4,     2,     3,     3,     3,     6,     5,     2,     2,
       1,     2,     2,     2,     1,     4,     3,     4,     3,     3,
       4,     4,     3,     2,     3,     3,     2,     3,     3,     4,
       6,     9,     2,     4,     1,     2,     3,     2,     3,     2,
       2,     3,     4,     2,     2,     1,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 8: /* line: abstractinterface  */
#line 105 "../src/parser.y"
                                              { properties.kind = ABSTRACTINTERFACE; properties.type = DEF;    }
#line 1771 "parser.cpp"
    break;

  case 9: /* line: assign  */
#line 106 "../src/parser.y"
                                              { properties.kind = ASSIGN;            properties.type = EXEC;   }
#line 1777 "parser.cpp"
    break;

  case 10: /* line: assignment  */
#line 107 "../src/parser.y"
                                              { properties.kind = ASSIGNMENT;        properties.type = EXEC;   }
#line 1783 "parser.cpp"
    break;

  case 11: /* line: associate  */
#line 108 "../src/parser.y"
                                              { properties.kind = ASSOCIATE;         properties.type = EXEC;   }
#line 1789 "parser.cpp"
    break;

  case 12: /* line: blank  */
#line 109 "../src/parser.y"
                                              { properties.kind = BLANK;             properties.type = NONE;   }
#line 1795 "parser.cpp"
    break;

  case 13: /* line: block  */
#line 110 "../src/parser.y"
                                              { properties.kind = BLOCK;             properties.type = EXEC;   }
#line 1801 "parser.cpp"
    break;

  case 14: /* line: blockdata  */
#line 111 "../src/parser.y"
                                              { properties.kind = BLOCKDATA;         properties.type = DEF;    }
#line 1807 "parser.cpp"
    break;

  case 15: /* line: call  */
#line 112 "../src/parser.y"
                                              { properties.kind = CALL;              properties.type = EXEC;   }
#line 1813 "parser.cpp"
    break;

  case 16: /* line: calllist  */
#line 113 "../src/parser.y"
                                              { properties.kind = CALLLIST;          properties.type = NONE;   }
#line 1819 "parser.cpp"
    break;

  case 17: /* line: case  */
#line 114 "../src/parser.y"
                                              { properties.kind = CASE;              properties.type = EXEC;   }
#line 1825 "parser.cpp"
    break;

  case 18: /* line: casedefault  */
#line 115 "../src/parser.y"
                                              { properties.kind = CASEDEFAULT;       properties.type = EXEC;   }
#line 1831 "parser.cpp"
    break;

  case 19: /* line: rank  */
#line 116 "../src/parser.y"
                                              { properties.kind = RANK;              properties.type = EXEC;   }
#line 1837 "parser.cpp"
    break;

  case 20: /* line: rankdefault  */
#line 117 "../src/parser.y"
                                              { properties.kind = RANKDEFAULT;       properties.type = EXEC;   }
#line 1843 "parser.cpp"
    break;

  case 21: /* line: changeteam  */
#line 118 "../src/parser.y"
                                              { properties.kind = CHANGETEAM;        properties.type = EXEC;   }
#line 1849 "parser.cpp"
    break;

  case 22: /* line: classdefault  */
#line 119 "../src/parser.y"
                                              { properties.kind = CLASSDEFAULT;      properties.type = EXEC;   }
#line 1855 "parser.cpp"
    break;

  case 23: /* line: classis  */
#line 120 "../src/parser.y"
                                              { properties.kind = CLASSIS;           properties.type = EXEC;   }
#line 1861 "parser.cpp"
    break;

  case 24: /* line: contains  */
#line 121 "../src/parser.y"
                                              { properties.kind = CONTAINS;          properties.type = DEF;    }
#line 1867 "parser.cpp"
    break;

  case 25: /* line: critical  */
#line 122 "../src/parser.y"
                                              { properties.kind = CRITICAL;          properties.type = DEF;    }
#line 1873 "parser.cpp"
    break;

  case 26: /* line: do_construct  */
#line 123 "../src/parser.y"
                                              { properties.kind = DO;                properties.type = EXEC;   }
#line 1879 "parser.cpp"
    break;

  case 27: /* line: else  */
#line 124 "../src/parser.y"
                                              { properties.kind = ELSE;              properties.type = EXEC;   }
#line 1885 "parser.cpp"
    break;

  case 28: /* line: elseif  */
#line 125 "../src/parser.y"
                                              { properties.kind = ELSEIF;            properties.type = EXEC;   }
#line 1891 "parser.cpp"
    break;

  case 29: /* line: elsewhere  */
#line 126 "../src/parser.y"
                                              { properties.kind = ELSEWHERE;         properties.type = EXEC;   }
#line 1897 "parser.cpp"
    break;

  case 30: /* line: endassociate  */
#line 127 "../src/parser.y"
                                              { properties.kind = ENDASSOCIATE;      properties.type = ENDDEF; }
#line 1903 "parser.cpp"
    break;

  case 31: /* line: endblock  */
#line 128 "../src/parser.y"
                                              { properties.kind = ENDBLOCK;          properties.type = EXEC;   }
#line 1909 "parser.cpp"
    break;

  case 32: /* line: endblockdata  */
#line 129 "../src/parser.y"
                                              { properties.kind = ENDBLOCKDATA;      properties.type = ENDDEF; }
#line 1915 "parser.cpp"
    break;

  case 33: /* line: endcritical  */
#line 130 "../src/parser.y"
                                              { properties.kind = ENDCRITICAL;       properties.type = ENDDEF; }
#line 1921 "parser.cpp"
    break;

  case 34: /* line: enddo  */
#line 131 "../src/parser.y"
                                              { properties.kind = ENDDO;             properties.type = EXEC;   }
#line 1927 "parser.cpp"
    break;

  case 35: /* line: endenum  */
#line 132 "../src/parser.y"
                                              { properties.kind = ENDENUM;           properties.type = DEF;    }
#line 1933 "parser.cpp"
    break;

  case 36: /* line: endforall  */
#line 133 "../src/parser.y"
                                              { properties.kind = ENDFORALL;         properties.type = EXEC;   }
#line 1939 "parser.cpp"
    break;

  case 37: /* line: endfunction  */
#line 134 "../src/parser.y"
                                              { properties.kind = ENDFUNCTION;       properties.type = ENDDEF; }
#line 1945 "parser.cpp"
    break;

  case 38: /* line: endif  */
#line 135 "../src/parser.y"
                                              { properties.kind = ENDIF;             properties.type = EXEC;   }
#line 1951 "parser.cpp"
    break;

  case 39: /* line: endinterface  */
#line 136 "../src/parser.y"
                                              { properties.kind = ENDINTERFACE;      properties.type = ENDDEF; }
#line 1957 "parser.cpp"
    break;

  case 40: /* line: endmodule  */
#line 137 "../src/parser.y"
                                              { properties.kind = ENDMODULE;         properties.type = ENDDEF; }
#line 1963 "parser.cpp"
    break;

  case 41: /* line: endprocedure  */
#line 138 "../src/parser.y"
                                              { properties.kind = ENDPROCEDURE;      properties.type = ENDDEF; }
#line 1969 "parser.cpp"
    break;

  case 42: /* line: endprogram  */
#line 139 "../src/parser.y"
                                              { properties.kind = ENDPROGRAM;        properties.type = ENDDEF; }
#line 1975 "parser.cpp"
    break;

  case 43: /* line: endselect  */
#line 140 "../src/parser.y"
                                              { properties.kind = ENDSELECT;         properties.type = EXEC;   }
#line 1981 "parser.cpp"
    break;

  case 44: /* line: endsubmodule  */
#line 141 "../src/parser.y"
                                              { properties.kind = ENDSUBMODULE;      properties.type = ENDDEF; }
#line 1987 "parser.cpp"
    break;

  case 45: /* line: endsubroutine  */
#line 142 "../src/parser.y"
                                              { properties.kind = ENDSUBROUTINE;     properties.type = ENDDEF; }
#line 1993 "parser.cpp"
    break;

  case 46: /* line: endteam  */
#line 143 "../src/parser.y"
                                              { properties.kind = ENDTEAM;           properties.type = EXEC;   }
#line 1999 "parser.cpp"
    break;

  case 47: /* line: endtype  */
#line 144 "../src/parser.y"
                                              { properties.kind = ENDTYPE;           properties.type = ENDDEF; }
#line 2005 "parser.cpp"
    break;

  case 48: /* line: endwhere  */
#line 145 "../src/parser.y"
                                              { properties.kind = ENDWHERE;          properties.type = EXEC;   }
#line 2011 "parser.cpp"
    break;

  case 49: /* line: entry  */
#line 146 "../src/parser.y"
                                              { properties.kind = ENTRY;             properties.type = DEF;    }
#line 2017 "parser.cpp"
    break;

  case 50: /* line: enum  */
#line 147 "../src/parser.y"
                                              { properties.kind = ENUM;              properties.type = DEF;    }
#line 2023 "parser.cpp"
    break;

  case 51: /* line: forall_construct  */
#line 148 "../src/parser.y"
                                              { properties.kind = FORALL;            properties.type = EXEC;   }
#line 2029 "parser.cpp"
    break;

  case 52: /* line: goto  */
#line 149 "../src/parser.y"
                                              { properties.kind = GOTO;              properties.type = EXEC;   }
#line 2035 "parser.cpp"
    break;

  case 53: /* line: goto_assigned  */
#line 150 "../src/parser.y"
                                              { properties.kind = GOTO_ASSIGNED;     properties.type = EXEC;   }
#line 2041 "parser.cpp"
    break;

  case 54: /* line: goto_computed  */
#line 151 "../src/parser.y"
                                              { properties.kind = GOTO_COMPUTED;     properties.type = EXEC;   }
#line 2047 "parser.cpp"
    break;

  case 55: /* line: if  */
#line 152 "../src/parser.y"
                                              { properties.kind = SIMPLEIF;          properties.type = EXEC;   }
#line 2053 "parser.cpp"
    break;

  case 56: /* line: if_construct  */
#line 153 "../src/parser.y"
                                              { properties.kind = IF;                properties.type = EXEC;   }
#line 2059 "parser.cpp"
    break;

  case 57: /* line: include  */
#line 154 "../src/parser.y"
                                              { properties.kind = INCLUDE;           properties.type = NONE;   }
#line 2065 "parser.cpp"
    break;

  case 58: /* line: interface  */
#line 155 "../src/parser.y"
                                              { properties.kind = INTERFACE;         properties.type = DEF;    }
#line 2071 "parser.cpp"
    break;

  case 59: /* line: labellist  */
#line 156 "../src/parser.y"
                                              { properties.kind = LABELLIST;         properties.type = NONE;   }
#line 2077 "parser.cpp"
    break;

  case 60: /* line: module  */
#line 157 "../src/parser.y"
                                              { properties.kind = MODULE;            properties.type = DEF;    }
#line 2083 "parser.cpp"
    break;

  case 61: /* line: moduleprocedure  */
#line 158 "../src/parser.y"
                                              { properties.kind = PROCEDURE;         properties.type = DEF;    }
#line 2089 "parser.cpp"
    break;

  case 62: /* line: nwayif  */
#line 159 "../src/parser.y"
                                              { properties.kind = NWAYIF;            properties.type = EXEC;   }
#line 2095 "parser.cpp"
    break;

  case 63: /* line: print  */
#line 160 "../src/parser.y"
                                              { properties.kind = PRINT;             properties.type = EXEC;   }
#line 2101 "parser.cpp"
    break;

  case 64: /* line: program_stmt  */
#line 161 "../src/parser.y"
                                              { properties.kind = PROGRAM;           properties.type = DEF;    }
#line 2107 "parser.cpp"
    break;

  case 65: /* line: procedure  */
#line 162 "../src/parser.y"
                                              { properties.kind = PROCEDURE;         properties.type = DEF;    }
#line 2113 "parser.cpp"
    break;

  case 66: /* line: read  */
#line 163 "../src/parser.y"
                                              { properties.kind = READ;              properties.type = EXEC;   }
#line 2119 "parser.cpp"
    break;

  case 67: /* line: io  */
#line 164 "../src/parser.y"
                                              { properties.kind = IO;                properties.type = EXEC;   }
#line 2125 "parser.cpp"
    break;

  case 68: /* line: iolist  */
#line 165 "../src/parser.y"
                                              { properties.kind = IOLIST;            properties.type = NONE;   }
#line 2131 "parser.cpp"
    break;

  case 69: /* line: selectcase  */
#line 166 "../src/parser.y"
                                              { properties.kind = SELECTCASE;        properties.type = EXEC;   }
#line 2137 "parser.cpp"
    break;

  case 70: /* line: selectrank  */
#line 167 "../src/parser.y"
                                              { properties.kind = SELECTRANK;        properties.type = EXEC;   }
#line 2143 "parser.cpp"
    break;

  case 71: /* line: selecttype  */
#line 168 "../src/parser.y"
                                              { properties.kind = SELECTTYPE;        properties.type = EXEC;   }
#line 2149 "parser.cpp"
    break;

  case 72: /* line: simple_end  */
#line 169 "../src/parser.y"
                                              { properties.kind = END;               properties.type = EXEC;   }
#line 2155 "parser.cpp"
    break;

  case 73: /* line: subroutine_stmt  */
#line 170 "../src/parser.y"
                                              { properties.kind = SUBROUTINE;        properties.type = DEF;    }
#line 2161 "parser.cpp"
    break;

  case 74: /* line: function_stmt  */
#line 171 "../src/parser.y"
                                              { properties.kind = FUNCTION;          properties.type = DEF;    }
#line 2167 "parser.cpp"
    break;

  case 75: /* line: submodule  */
#line 172 "../src/parser.y"
                                              { properties.kind = SUBMODULE;         properties.type = DEF;    }
#line 2173 "parser.cpp"
    break;

  case 76: /* line: type  */
#line 173 "../src/parser.y"
                                              { properties.kind = TYPE;              properties.type = DEF;    }
#line 2179 "parser.cpp"
    break;

  case 77: /* line: typeis  */
#line 174 "../src/parser.y"
                                              { properties.kind = TYPEIS;            properties.type = EXEC;   }
#line 2185 "parser.cpp"
    break;

  case 78: /* line: use  */
#line 175 "../src/parser.y"
                                              { properties.kind = USE;               properties.type = DEF;    }
#line 2191 "parser.cpp"
    break;

  case 79: /* line: where_construct  */
#line 176 "../src/parser.y"
                                              { properties.kind = WHERE;             properties.type = EXEC;   }
#line 2197 "parser.cpp"
    break;

  case 80: /* line: segment  */
#line 177 "../src/parser.y"
                                              { properties.kind = SEGMENT;           properties.type = EXEC;   }
#line 2203 "parser.cpp"
    break;

  case 81: /* line: endsegment  */
#line 178 "../src/parser.y"
                                              { properties.kind = ENDSEGMENT;        properties.type = EXEC;   }
#line 2209 "parser.cpp"
    break;

  case 87: /* include: INCLUDE QSTRING getstring EOL  */
#line 190 "../src/parser.y"
                                                       {D(O("include"););}
#line 2215 "parser.cpp"
    break;

  case 191: /* $@1: %empty  */
#line 387 "../src/parser.y"
                                 {itemnumber++;}
#line 2221 "parser.cpp"
    break;

  case 193: /* $@2: %empty  */
#line 388 "../src/parser.y"
                                 {itemnumber=123456;}
#line 2227 "parser.cpp"
    break;

  case 195: /* $@3: %empty  */
#line 389 "../src/parser.y"
                                 {itemnumber=123456;}
#line 2233 "parser.cpp"
    break;

  case 197: /* item: otheritem  */
#line 390 "../src/parser.y"
                                   {itemnumber++;}
#line 2239 "parser.cpp"
    break;

  case 246: /* enable_identifier: %empty  */
#line 482 "../src/parser.y"
                         {lexer_enable(IDENTIFIER);}
#line 2245 "parser.cpp"
    break;

  case 247: /* enable_cidentifier: %empty  */
#line 484 "../src/parser.y"
                         {lexer_enable(CIDENTIFIER);}
#line 2251 "parser.cpp"
    break;

  case 248: /* enable_skip: %empty  */
#line 486 "../src/parser.y"
                         {lexer_enable(SKIP);}
#line 2257 "parser.cpp"
    break;

  case 249: /* enable_skipall: %empty  */
#line 488 "../src/parser.y"
                         {lexer_enable(SKIPALL);}
#line 2263 "parser.cpp"
    break;

  case 250: /* enable_skipnoop: %empty  */
#line 490 "../src/parser.y"
                         {lexer_enable(SKIPNOOP);}
#line 2269 "parser.cpp"
    break;

  case 251: /* enable_skipnos: %empty  */
#line 492 "../src/parser.y"
                         {lexer_enable(SKIPNOS);}
#line 2275 "parser.cpp"
    break;

  case 252: /* getname: %empty  */
#line 494 "../src/parser.y"
                         {properties.name=lexer_getname();}
#line 2281 "parser.cpp"
    break;

  case 253: /* getstlabel: %empty  */
#line 496 "../src/parser.y"
                         {properties.stlabel=lexer_getstlabel();}
#line 2287 "parser.cpp"
    break;

  case 254: /* getlabel: %empty  */
#line 498 "../src/parser.y"
                         {properties.labels.push_back(lexer_geti_number());
	                  properties.label_pos.push_back(lexer_get_pos()-lexer_get_len());}
#line 2294 "parser.cpp"
    break;

  case 255: /* getlabel1: %empty  */
#line 501 "../src/parser.y"
                         {if (itemnumber == 2)
	                    {properties.labels.push_back(lexer_geti_number());
	                     properties.label_pos.push_back(lexer_get_pos()-lexer_get_len());
			     }
			  }
#line 2304 "parser.cpp"
    break;

  case 256: /* getlabel2: %empty  */
#line 507 "../src/parser.y"
                          {properties.labels.push_back(lexer_geti_number());
	                   /* minus one for the extra comma, plus two for preceding ',*' */
	                  properties.label_pos.push_back(lexer_get_pos()-lexer_get_len()-1+2);}
#line 2312 "parser.cpp"
    break;

  case 257: /* getstring: %empty  */
#line 511 "../src/parser.y"
                         {properties.stringvalue=lexer_getstring();}
#line 2318 "parser.cpp"
    break;

  case 258: /* getlr: %empty  */
#line 513 "../src/parser.y"
                         {properties.lrvalue=lexer_getlr();
                          properties.lrpos=lexer_getlrpos();}
#line 2325 "parser.cpp"
    break;


#line 2329 "parser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 518 "../src/parser.y"


void resetprop(void)
{
   properties.kind              = UNCLASSIFIED;
   properties.type              = EXEC;
   properties.labels.clear();
   properties.label_pos.clear();
   properties.stlabel           = "";
   properties.name              = "";
   properties.stringvalue       = "";
   properties.lrvalue           = "";
   properties.lrpos             = -123;
}

propstruct parseline(const Line_prep &p)
{
// It should be noted that parsing is somewhat sloppy.
// The intention is, that indent-influencing statements are recognized
// and that labels (GOTO, ASSIGN, ..) are correctly identified
// without giving false positives, such as in
//  IF(L) GOTO 1 = 4
// (which went wrong in a previous version).

   D(O("entering parseline"););
   D(O("line:");O(p.get_line()););

   lexer_set(p,IDENTIFIER);
   resetprop();
   yyparse();
   D(O(t2n(properties.kind)););
   if (properties.kind != UNCLASSIFIED)
      return properties;
#ifdef USEESOPE
   lexer_set(p,ESOPE);    // enables KEYWORD+ESOPE (SEGMENT, ENDSEGMENT)
#else
   lexer_set(p,KEYWORD);  // enables KEYWORD
#endif
   yyparse();
   D(O(t2n(properties.kind)););
   return properties;
}

propstruct parselabels(const std::string &s)
{
   D(O("entering parselabels"););
   lexer_set(s,LABELS);
   resetprop();
   parselabeling = true;
   yyparse();
   return properties;
}

propstruct parseio(const std::string &s)
{
   D(O("entering parseio"););
   lexer_set(s,IOLIST);
   resetprop();
   parselabeling = true;
   itemnumber = 0;
   yyparse();
   return properties;
}

propstruct parsecall(const std::string &s)
{
   D(O("entering parsecall"););
   lexer_set(','+s,CALLLIST);    // ',' to assist lexer to recognise first label
   resetprop();
   parselabeling = true;
   yyparse();
   return properties;
}

void yyerror(const char *c)
{
    (void)c;
    D(O("ERROR");O(c););
    if(parselabeling)
    {
       properties.kind = UNCLASSIFIED;
       properties.type = EXEC;
       }
}

extern "C" int yywrap()
{
  D(O("yywrap"););
  return 1;
}
