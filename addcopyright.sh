#!/bin/sh
# -copyright-
#-# Copyright: 2015-2025 Willem Vermin wvermin@gmail.com
#-# 
#-# License: BSD-3-Clause
#-#  Redistribution and use in source and binary forms, with or without
#-#  modification, are permitted provided that the following conditions
#-#  are met:
#-#  1. Redistributions of source code must retain the above copyright
#-#     notice, this list of conditions and the following disclaimer.
#-#  2. Redistributions in binary form must reproduce the above copyright
#-#     notice, this list of conditions and the following disclaimer in the
#-#     documentation and/or other materials provided with the distribution.
#-#  3. Neither the name of the copyright holder nor the names of its
#-#     contributors may be used to endorse or promote products derived
#-#     from this software without specific prior written permission.
#-#   
#-#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#-#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#-#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#-#  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
#-#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#-#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#-#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#-#  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
#-#  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#-#  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#-#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

crfile=`mktemp`
crfilevim=`mktemp`
cat < COPYING  | sed 's/^/#-# /' > "$crfile"
cat < COPYING  | sed 's/^/"#-# /' > "$crfilevim"

n=0
tmpfile="tmpfile"
while [ "$1" ] ; do
   f="$1"
   shift
   txt="-""copyright-"
   if file --mime "$f" | grep -q binary ; then
      echo "$f: binary"
      continue
   fi
   if ! grep -q -- "$txt" "$f" ; then
      echo "$f: no $txt"
      continue
   fi
   # following 2 sed commands take care that only the first '-copyright-'
   # will be followed by the copyright text
   # Notice that sed requires a newline after the filename of the 'r' command
   # Not using 'sed -i' because not all sed's interpret this the same
   case "$f" in
      *.vim)
          sed "/^[ 	]*\"#-#/d" "$f" > "$tmpfile" && cp "$tmpfile" "$f"
	  cr=$crfilevim
	 ;;
      *)
          sed "/^[ 	]*#-#/d" "$f" > "$tmpfile" && cp "$tmpfile" "$f"
	  cr=$crfile
         ;;
   esac

   sed "/$txt/{r $cr
:a
n
ba
}" "$f" > "$tmpfile" && cp "$tmpfile" "$f"
   n=`expr $n + 1`
done
echo "$n files copyrighted"
rm -f "$tmpfile" "$crfile" "$crfilevim"
