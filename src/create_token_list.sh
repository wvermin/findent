#!/bin/sh
# create headerfile containing an array with the token names
# created by bison in parser.hpp (a.k.a. y.tab.h)
# Since this depends heavily on the exact format of this file,
# an usable headerfile is only created if FINDENT_CREATE_TOKEN_LIST
# is set. In this file macrp HAVE_TOKEN_LIST is defined.
# If FINDENT_CREATE_TOKEN_LIST is not set, an almost empty headerfile
# is produced.
#
a="$1"   # input
f="$2"   # output
echo "#ifndef TOKEN2NAME_H" > "$f"
echo "#define TOKEN2NAME_H" >> "$f"
echo "std::string tokenlist[] = {" >> "$f"
if [ ! -r "$a" -o -z "$FINDENT_CREATE_TOKEN_LIST" ] ; then
   echo "};" >> "$f"
   echo "int firsttoken = 0;" >> "$f"
   echo "int lasttoken = 0;" >> "$f"
   echo "#endif" >> "$f"
   exit
fi

echo "#define HAVE_TOKEN_LIST" >> "$f"

awk < "$a" >> "$f" '
START { state = 0 ; n = 0; s = 0}
/enum yytokentype/ { state = 1 }
/{/ {if (state == 1) { state = 2;next}}
state == 2 {if (index($0,";")) exit
             name = $1; number = $3; sub(",","",number)
	     if (number > 0)
	       {
		  n++;
		  print "\""name"\","
		  if (s == 0) s = number;
		  e = number;
	       }
	    }
END { print "};"
      print "int firsttoken =",s,";"
      print "int lasttoken  =",e,";"
   }
'

echo "#endif" >> "$f"
